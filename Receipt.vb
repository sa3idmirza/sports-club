﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class Receipt

    Private Sub Receipt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()

        Dim command As SqlCommand = New SqlCommand("Select MAX(Rec_ID) + 1 from Receipts", conx)
        command.CommandType = CommandType.Text
        ID_txt.Text = command.ExecuteScalar.ToString

        ID_lbl.Text = Main.user_ID.ToString

        Dim command1 As SqlCommand = New SqlCommand("Select Emp_name from Employee where Emp_ID = '" + ID_lbl.Text + "'", conx)
        command1.CommandType = CommandType.Text
        name_txt.Text = command1.ExecuteScalar.ToString

        Date_txt.Text = Date.Now.Month.ToString + "/" + Date.Now.Day.ToString + "/" + Date.Now.Year.ToString

    End Sub

    Private Sub Cust_name_txt_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Cust_name_txt.MouseDoubleClick
        Search.MdiParent = Main
        Search.Show()
        Search.str = "select Cust_ID As 'ID' , Cust_name as 'Name' ,Cust_lastname as 'Last Name' , Cust_balance as 'Balance'   from Customer_member where Cust_name like '%"

        Search.fromRec = True
    End Sub

    Private Sub Cust_name_txt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cust_name_txt.TextChanged
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()

        Dim command As SqlCommand = New SqlCommand("Select Cust_balance from Customer_member where Cust_ID ='" + Cust_ID_lbl.Text + "'", conx)
        command.CommandType = CommandType.Text
        amount_txt.Text = command.ExecuteScalar.ToString
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()

        Dim sqlcommand As New SqlCommand("Insert Into Receipts(Rec_amount,Rec_date,Cust_ID,Emp_ID) values('" + Payedamount_txt.Text + "' , '" + Date_txt.Text + "','" + Cust_ID_lbl.Text + "','" + ID_lbl.Text + "')", conx)
        sqlcommand.CommandType = CommandType.Text
        sqlcommand.ExecuteNonQuery()
        conx.Close()
        Me.Receipt_Load(sender, e)
        name_txt.Clear()
        Payedamount_txt.Clear()
        amount_txt.Clear()
        Cust_name_txt.Clear()

    End Sub
End Class