﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class New_Invoice
    Public ID As Integer
    Dim totit As Integer
    Dim rowdel As Integer

   
    Private Sub New_Invoice_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If MessageBox.Show("BE CAREFULL !! Pease if there is items in the list ,to leave cancel it ! Exiting without canceling will ruin the databse!! ", "Close", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
        Else
            e.Cancel = True

        End If
    End Sub
   
    Private Sub New_Invoice_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        If (Main.adminis = False) Then
            Discount_Txt.Enabled = False
        End If
        Dim command As SqlCommand = New SqlCommand("Select MAX(Inv_ID) + 1 from Invoices", conx)
        command.CommandType = CommandType.Text
        ID_txt.Text = command.ExecuteScalar.ToString

        ID_lbl.Text = Main.user_ID.ToString

        Dim command1 As SqlCommand = New SqlCommand("Select Emp_name from Employee where Emp_ID = '" + ID_lbl.Text + "'", conx)
        command1.CommandType = CommandType.Text
        name_txt.Text = command1.ExecuteScalar.ToString
        Date_txt.Text = Date.Now.Month.ToString + "/" + Date.Now.Day.ToString + "/" + Date.Now.Year.ToString
    End Sub

    Private Sub Cust_name_txt_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Cust_name_txt.MouseDoubleClick
        Search.MdiParent = Main
        Search.Show()
        Search.str = "select Cust_ID As 'ID' , Cust_name as 'Name' ,Cust_lastname as 'Last Name'   from Customer_member where Cust_name like '%"
        Search.frominvcust = True
    End Sub

     
    Private Sub reservation_check_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reservation_check.CheckedChanged
        If (reservation_check.Checked) Then
            qtty_txt.Text = 1
            qtty_txt.ReadOnly = True
        Else
            qtty_txt.Text = 0
            qtty_txt.ReadOnly = False
        End If
        
    End Sub

    Private Sub Item_txt_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Item_txt.MouseDoubleClick
        If (reservation_check.Checked) Then
            Search.MdiParent = Main
            Search.Show()
            Search.str = "exec selectresbydate "
            Search.frominvRes = True
            Search.Date_pick.Visible = True
            Search.date_lbl.Visible = True
            Search.CheckBox1.Visible = True

        Else
            Search.MdiParent = Main
            Search.Show()
            Search.str = "select Item_ID As 'ID' , Item_desc as 'Name' , Item_qtty as 'Quantity' , Item_price as 'Price' , Item_cost as 'Cost' from Items where Item_desc like '%"
            Search.frominvItem = True
        End If
    End Sub

    Private Sub Item_txt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Item_txt.TextChanged

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim strquery As String
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim comnd As New SqlCommand("select max(inv_det_ID) +1 from invoice_Detail", conx)
        comnd.CommandType = CommandType.Text

        totit = Integer.Parse(qtty_txt.Text) * Integer.Parse(cost_txt.Text)
        If (reservation_check.Checked) Then

            DataGridView1.Rows.Add(New String() {"Reservation", comnd.ExecuteScalar.ToString, ID.ToString, Item_txt.Text, qtty_txt.Text, cost_txt.Text, totit.ToString})
            strquery = "insert into Invoice_detail (qtty,Res_ID,Inv_det_price) values('" + qtty_txt.Text + "','" + ID.ToString + "','" + totit.ToString + "')"

            Dim command2 As SqlCommand = New SqlCommand("update reservation set Paid_flag='yes' where Res_ID = '" + ID.ToString + "'", conx)
            command2.CommandType = CommandType.Text
            command2.ExecuteNonQuery()
        Else
            If (qtty_txt.Text > max_lbl.Text) Then
                MsgBox("please quantity does not exitst ")
                Exit Sub
            End If
            DataGridView1.Rows.Add(New String() {"Item", comnd.ExecuteScalar.ToString, ID.ToString, Item_txt.Text, qtty_txt.Text, cost_txt.Text, totit.ToString})
            strquery = "insert into Invoice_detail (qtty,Item_ID,Inv_det_price) values('" + qtty_txt.Text + "','" + ID.ToString + "','" + totit.ToString + "')"

        End If


        Dim command As SqlCommand = New SqlCommand(strquery, conx)
        command.CommandType = CommandType.Text
        command.ExecuteNonQuery()
        conx.Close()

        total_txt.Text = Integer.Parse(total_txt.Text) + totit.ToString


        totalnet_txt.Text = Double.Parse(total_txt.Text) - Double.Parse(total_txt.Text) * Double.Parse(Discount_Txt.Text) / 100
        Item_txt.Clear()
        cost_txt.Text = 0
        If (reservation_check.Checked) Then
            qtty_txt.Text = 1
        Else
            qtty_txt.Text = 0
        End If

        max_lbl.Text = " "

    End Sub

    Private Sub Discount_Txt_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Discount_Txt.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(Keys.Return) Then


            totalnet_txt.Text = Double.Parse(totalnet_txt.Text) - Double.Parse(totalnet_txt.Text) * Double.Parse(Discount_Txt.Text) / 100

        End If
    End Sub
    Private Sub DataGridView1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridView1.MouseUp

        rowdel = DataGridView1.CurrentCell.RowIndex
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()

        Dim id As Integer
        id = DataGridView1.Item(1, rowdel).Value
        total_txt.Text = Integer.Parse(total_txt.Text) - Integer.Parse(DataGridView1.Item(6, rowdel).Value)
          totalnet_txt.Text = Double.Parse(total_txt.Text) - Double.Parse(total_txt.Text) * Double.Parse(Discount_Txt.Text) / 100
        If (DataGridView1.Item(0, rowdel).Value.ToString = "Reservation") Then
            Dim command2 As SqlCommand = New SqlCommand("update reservation set Paid_flag='no' where Res_ID = '" + DataGridView1.Item(2, rowdel).Value.ToString + "'", conx)
            command2.CommandType = CommandType.Text


            command2.ExecuteNonQuery()
        End If
        DataGridView1.Rows.RemoveAt(rowdel)

        Dim command1 As SqlCommand = New SqlCommand(" DELETE FROM Invoice_detail WHERE Inv_det_ID='" + id.ToString + "'", conx)
        command1.CommandType = CommandType.Text
        command1.ExecuteNonQuery()
        conx.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim conx As SqlConnection

        conx = conSQL()
        conx.Open()


        Dim command As SqlCommand = New SqlCommand("insertintoinvoice", conx)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@date", Date_txt.Text)
        command.Parameters.AddWithValue("@amount", total_txt.Text)
        command.Parameters.AddWithValue("@disc", Discount_Txt.Text)
        command.Parameters.AddWithValue("@cust_ID", Cust_ID_lbl.Text)
        command.Parameters.AddWithValue("@emp_ID", ID_lbl.Text)
        command.Parameters.AddWithValue("@tot", Double.Parse(totalnet_txt.Text))
        command.ExecuteNonQuery()


        Dim x As Integer = 0
        For Each row As DataGridViewRow In DataGridView1.Rows
            
                Dim command1 As SqlCommand = New SqlCommand("updateinvoicedet", conx)
                command1.CommandType = CommandType.StoredProcedure
            command1.Parameters.AddWithValue("@Inv_ID", ID_txt.Text)
            command1.Parameters.AddWithValue("@Id", DataGridView1.Item(1, x).Value.ToString)

                command1.ExecuteNonQuery()
                x = x + 1
           

        Next
        conx.Close()
        Me.New_Invoice_Load(sender, e)
        Cust_name_txt.Clear()
        Cust_ID_lbl.Text = " "
        Item_txt.Clear()
        qtty_txt.Text = 0
        cost_txt.Text = 0
        total_txt.Text = 0
        totalnet_txt.Text = 0
        Discount_Txt.Text = 0

        DataGridView1.Rows.Clear()


    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim x As Integer = 0
        For Each row As DataGridViewRow In DataGridView1.Rows
            Dim command1 As SqlCommand = New SqlCommand(" DELETE FROM Invoice_detail WHERE Inv_det_ID='" + DataGridView1.Item(1, rowdel).Value.ToString + "'", conx)
            command1.CommandType = CommandType.Text
            command1.ExecuteNonQuery()
        Next
        conx.Close()
        Me.New_Invoice_Load(sender, e)
        Cust_name_txt.Clear()
        Cust_ID_lbl.Text = " "
        Item_txt.Clear()
        qtty_txt.Text = 0
        cost_txt.Text = 0
        total_txt.Text = 0
        totalnet_txt.Text = 0
        Discount_Txt.Text = 0

        DataGridView1.Rows.Clear()
    End Sub

    Private Sub Cust_name_txt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cust_name_txt.TextChanged
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim command1 As SqlCommand = New SqlCommand("getdisc", conx)
        command1.CommandType = CommandType.StoredProcedure

        command1.Parameters.AddWithValue("@Cust_id", Cust_ID_lbl.Text)

        Discount_Txt.Text = command1.ExecuteScalar.ToString
        
        totalnet_txt.Text = Double.Parse(total_txt.Text) - Double.Parse(total_txt.Text) * Double.Parse(Discount_Txt.Text) / 100

    End Sub
End Class