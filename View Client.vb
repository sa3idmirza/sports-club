﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO

Public Class View_Client

    Public Sub View_Client_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Sports_clubDataSet8.Status_cust' table. You can move, or remove it, as needed.
        Me.Status_custTableAdapter1.Fill(Me.Sports_clubDataSet8.Status_cust)
        If (Main.adminis = False) Then
            Stat_lbl.Visible = False
            Statuscombo.Visible = False


        End If
        Me.Status_custTableAdapter.Fill(Me.Sports_clubDataSet.Status_cust)

        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim strquery As String
        strquery = "select Cust_ID as 'ID' , Cust_name as 'First Name' , Cust_lastname as 'Last Name'from Customer_member "
        da = New SqlDataAdapter(strquery, conx)
        da.Fill(ds, "Customer_member")
        t = ds.Tables(0)
        DataGridView2.DataSource = t
        Me.client_combo_SelectedIndexChanged(sender, e)
        conx.Close()
    End Sub
    Public Sub client_combo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles client_combo.SelectedIndexChanged
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim strquery As String
        Dim strquery2 As String

        strquery = "select Cust_ID as 'ID',Cust_name as 'First name', Cust_lastname as 'Last name', Cust_balance as 'Balance' , Cust_overall as 'overall' from Customer_member"
        strquery2 = "select Count(Cust_ID) from Customer_member "
        If (client_combo.Text <> "ALL") Then
            strquery += " where Status_ID = " + client_combo.SelectedValue.ToString
            strquery2 += "where Status_ID = '" + client_combo.SelectedValue.ToString + "'"
        End If
        Try
            Dim cmd As SqlCommand = New SqlCommand(strquery2, conx)
            cmd = New SqlCommand(strquery2, conx)
            cmd.CommandType = CommandType.Text
            count_lbl.Text = "Record Count : " + cmd.ExecuteScalar().ToString
            'fill the datagridview 
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Customer_member")
            t = ds.Tables(0)
            DataGridView1.DataSource = t
        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()

        End Try

    End Sub


    Public Sub fillfield(ByVal i As Integer)
        Try
            Dim r As DataRow
            Dim da As New SqlDataAdapter
            Dim ds As New DataSet
            Dim t As DataTable
            Dim conx As SqlConnection
            conx = conSQL()
            conx.Open()
            Dim strquery As String
            strquery = "select * from Customer_member "
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Customer_member")
            t = ds.Tables(0)

            r = t.Rows(i)
            ID_lbl.Text = r.Item("Cust_ID")
            name_txt.Text = r.Item("Cust_name")
            lname_txt.Text = r.Item("Cust_lastname")
            cell_txt.Text = r.Item("Cust_cell")
            Statuscombo.SelectedValue = r.Item("Status_ID")
            conx.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub





    Private Sub DataGridView2_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridView2.MouseUp
        Dim x As Integer
        Try
            x = DataGridView2.CurrentCell.RowIndex
            ID_lbl.Text = x.ToString
            fillfield(x)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub



    Private Sub DataGridView1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridView1.MouseUp
        Dim x As Integer
        Try
            x = DataGridView1.CurrentRow.Index
            DataGridView6.DataSource = Nothing
            getID(x)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub getID(ByVal i As Integer)
        Try
            Dim rowindex As Integer = 0
            Dim ID As Integer
            ID = DataGridView1.Item(0, i).Value
            filldatagridview3(ID)
            filldatagridview4(ID)
            filldatagridview5(ID)
            For Each row As DataGridViewRow In DataGridView2.Rows

                If row.Cells.Item("ID").Value = ID Then
                    Exit For


                End If
                rowindex = rowindex + 1
            Next
            DataGridView2.Rows(rowindex).Selected = True
            ' DataGridView2.VerticalScrollingOffset(
            fillfield(rowindex)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Function filldatagridview3(ByVal ID As Integer)
        Dim conx As SqlConnection
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable

        conx = conSQL()

        conx.Open()
        Try


            Dim strquery As String
            strquery = "select Inv_ID as 'Reference' , Inv_date as 'Issue Date' , Inv_amount as 'Amount' , Inv_discount as 'Discount %' , total_net as 'Total' from Invoices  where  Cust_ID =  '" + ID.ToString + "'"
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Invoices")
            t = ds.Tables(0)
            DataGridView3.DataSource = t

        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try
    End Function

    Function filldatagridview4(ByVal ID As Integer)
        Dim conx As SqlConnection
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable

        conx = conSQL()

        conx.Open()
        Try


            Dim strquery As String
            strquery = "select Rec_ID as 'Reference' , Rec_date as 'Issue Date' , Rec_amount as 'Amount'  from Receipts  where  Cust_ID =  '" + ID.ToString + "'"
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Receipts")
            t = ds.Tables(0)
            DataGridView4.DataSource = t

        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try
    End Function
    Function filldatagridview5(ByVal ID As Integer)
        Dim conx As SqlConnection
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable

        conx = conSQL()

        conx.Open()
        Try


            Dim strquery As String
            strquery = "select Res_ID as 'Reference' , Res_date as 'Reservation Date' , Cancel_flag as 'Canceled' ,Res_hour_beg as 'At Time',Res_hour_end as 'To Time',Paid_flag as 'Paid'    from Reservation  where  Cust_ID =  '" + ID.ToString + "'   and Cancel_flag = 'no'"
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Invoices")
            t = ds.Tables(0)
            DataGridView5.DataSource = t

        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try
    End Function


    Public Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim conx As SqlConnection
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable

        conx = conSQL()

        conx.Open()
        Try
            Dim command As SqlCommand = New SqlCommand("UpdateCust", conx)
            command.CommandType = CommandType.StoredProcedure

            command.Parameters.AddWithValue("@id", ID_lbl.Text)
            command.Parameters.AddWithValue("@name", name_txt.Text)
            command.Parameters.AddWithValue("@lastname", lname_txt.Text)
            command.Parameters.AddWithValue("@cell", cell_txt.Text)
            If (Main.adminis) Then
                command.Parameters.AddWithValue("@stat", Statuscombo.SelectedValue)
            Else
                command.Parameters.AddWithValue("@stat", 1)
            End If
            command.ExecuteNonQuery()

            Dim strquery As String
            strquery = "select Cust_ID as 'ID' , Cust_name as 'First Name' , Cust_lastname as 'Last Name' from Customer_member "
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Customer_member")
            t = ds.Tables(0)
            DataGridView2.DataSource = t
            client_combo_SelectedIndexChanged(sender, e)
        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
          New_Client.MdiParent = Main
        New_Client.Show()
    End Sub
     
    Private Sub DataGridView3_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView3.CellContentClick

    End Sub

    Private Sub DataGridView3_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridView3.MouseDoubleClick
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable
        Dim conx As SqlConnection
        Dim x As Integer
        x = DataGridView3.CurrentCell.RowIndex
        conx = conSQL()
        conx.Open()
        Dim strquery As String
        strquery = "EXEC selectinvdet '" + DataGridView3.Item(0, x).Value.ToString + "'"

        ds.Clear()
        da = New SqlDataAdapter(strquery, conx)
        da.Fill(ds)
        t = ds.Tables(0)
        DataGridView6.DataSource = t
        conx.Close()
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles search_txt.TextChanged
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        ds.Clear()
        Dim strquery As String
        Dim strquery2 As String
        strquery = "select Cust_ID as 'ID',Cust_name as 'First name', Cust_lastname as 'Last name', Cust_balance as 'Balance' , Cust_overall as 'overall' from Customer_member  where "
        strquery2 = "select Count(Cust_ID) from Customer_member where "

        strquery += " Status_ID = " + client_combo.SelectedValue.ToString + " and "
        strquery2 += " Status_ID = '" + client_combo.SelectedValue.ToString + "' and "

        strquery += "  Cust_name like '%" + search_txt.Text + "%'"
        strquery2 += "  Cust_name like '%" + search_txt.Text + "%'"
        da = New SqlDataAdapter(strquery, conx)
        da.Fill(ds)
        t = ds.Tables(0)
        DataGridView1.DataSource = t
        Dim sqlcom As SqlCommand = New SqlCommand(strquery2, conx)
        sqlcom.CommandType = CommandType.Text
        count_lbl.Text = sqlcom.ExecuteScalar.ToString
        conx.Close()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim rowdel As Integer
        Dim id As Integer
        id = DataGridView1.Item(0, rowdel).Value
         
        DataGridView1.Rows.RemoveAt(rowdel)
        Dim command1 As SqlCommand = New SqlCommand(" DELETE FROM Reservation WHERE Res_ID='" + id.ToString + "'", conx)
        command1.CommandType = CommandType.Text
        command1.ExecuteNonQuery()


        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable

        
        Try


            Dim strquery As String
            strquery = "select Res_ID as 'Reference' , Res_date as 'Reservation Date' , Cancel_flag as 'Canceled' ,Res_hour_beg as 'At Time',Res_hour_end as 'To Time'    from Reservation  where  Cust_ID =  '" + id.ToString + "' and Res_date >='" + Date.Now.Month.ToString + "/" + Date.Now.Day.ToString + "/" + Date.Now.Year.ToString + "' and Cancel_flag = 'no'"
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Invoices")
            t = ds.Tables(0)
            DataGridView5.DataSource = t

        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try
    End Sub

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        ds.Clear()
        Dim strquery As String
        Dim strquery2 As String
        strquery = "select Cust_ID as 'ID',Cust_name as 'First name', Cust_lastname as 'Last name', Cust_balance as 'Balance' , Cust_overall as 'overall' from Customer_member    "
        strquery2 = "select Count(Cust_ID) from Customer_member  "
        If (CheckBox1.Checked = True) Then
            client_combo.Enabled = False
            search_txt.Enabled = False

           
        Else
            strquery += " where Status_ID = " + client_combo.SelectedValue.ToString + " and "
            strquery2 += "where  Status_ID = '" + client_combo.SelectedValue.ToString + "' and "

            strquery += "  Cust_name like '%" + search_txt.Text + "%'"
            strquery2 += "  Cust_name like '%" + search_txt.Text + "%'"
            client_combo.Enabled = True
            search_txt.Enabled = True
        End If
        da = New SqlDataAdapter(strquery, conx)
        da.Fill(ds)
        t = ds.Tables(0)
        DataGridView1.DataSource = t

        Dim sqlcom As SqlCommand = New SqlCommand(strquery2, conx)
        sqlcom.CommandType = CommandType.Text
        count_lbl.Text = sqlcom.ExecuteScalar.ToString
        conx.Close()
    End Sub
End Class