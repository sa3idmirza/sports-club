﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class New_Client

    Private Sub New_Client_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Sports_clubDataSet10.Status_cust' table. You can move, or remove it, as needed.
        Me.Status_custTableAdapter1.Fill(Me.Sports_clubDataSet10.Status_cust)
        If (Main.adminis = False) Then
            Stat_lbl.Visible = False
            Me.Status_custTableAdapter.Fill(Me.Sports_clubDataSet1.Status_cust)
            Statuscombo.Visible = False
        End If


        Dim conx As New SqlConnection
        conx = conSQL()
        conx.Open()
        Try
            Dim cmd As SqlCommand = New SqlCommand("SELECT MAX (Cust_ID) + 1 FROM Customer_member ", conx)
            cmd.CommandType = CommandType.Text
            ID_lbl.Text = cmd.ExecuteScalar().ToString
        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If name_txt.TextLength = 0 Or lname_txt.TextLength = 0 Then
            MsgBox("fill the required fields correctly !!")
            ErrorProvider1.SetError(name_txt, "This field is a required field")
            ErrorProvider1.SetError(lname_txt, "This field is a required field")
            ErrorProvider1.SetError(cell_txt, "this field shoud be in the form xx-xxxxxx example 01-123142")
        Else

            Dim conx As SqlConnection



            conx = conSQL()

            conx.Open()
            Try
                Dim command As SqlCommand = New SqlCommand("insertintocust", conx)
                command.CommandType = CommandType.StoredProcedure


                command.Parameters.AddWithValue("@name", name_txt.Text)
                command.Parameters.AddWithValue("@lastname", lname_txt.Text)
                command.Parameters.AddWithValue("@cell", cell_txt.Text)
                If (Main.adminis) Then
                    command.Parameters.AddWithValue("@stat", Statuscombo.SelectedValue)
                Else
                    command.Parameters.AddWithValue("@stat", 2)
                End If
                command.ExecuteNonQuery()
                name_txt.Clear()
                lname_txt.Clear()
                cell_txt.Clear()
                Me.New_Client_Load(sender, e)
                For Each frm As Form In Application.OpenForms
                    If frm.Name.Equals("View_Client") Then
                        View_Client.client_combo_SelectedIndexChanged(sender, e)
                        View_Client.Button1_Click(sender, e)
                    End If
                  
                Next
            Catch ex As Exception
                MsgBox(ex.Message)
                Throw
            Finally
                conx.Close()
            End Try

        End If
    End Sub
End Class