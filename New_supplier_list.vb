﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class New_supplier_list
    Public ID As Integer
    Dim rowdel As Integer
    Private Sub New_supplier_list_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        total_txt.Text = 0
        Dim command As SqlCommand = New SqlCommand("Select MAX(Supplies_ID) + 1 from Supplies", conx)
        command.CommandType = CommandType.Text
        ID_txt.Text = command.ExecuteScalar.ToString

        Date_txt.Text = Date.Now.Month.ToString + "/" + Date.Now.Day.ToString + "/" + Date.Now.Year.ToString
    End Sub

    Private Sub name_txt_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles name_txt.MouseDoubleClick
        Search.MdiParent = Main
        Search.Show()
        Search.str = "select Supplier_ID As 'ID' , Supplier_name as 'Name' , Supplier_cell as 'Cell Phone'  , Supplier_add as 'Address' from Suppliers where Supplier_name like '%"

        Search.fromsupList = True
    End Sub

    
    Private Sub Item_txt_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Item_txt.MouseDoubleClick
        Search.MdiParent = Main
        Search.Show()
        Search.str = "select Item_ID As 'ID' , Item_desc as 'Name' , Item_qtty as 'Quantity' , Item_price as 'Price' , Item_cost as 'Cost' from Items where Item_desc like '%"
        Search.fromSuplistItem = True
    End Sub
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim command As SqlCommand = New SqlCommand("select MAX(supp_det_ID)+1 from Supp_det", conx)
        command.CommandType = CommandType.Text
        Dim total As Double
        total = ((Integer.Parse(cost_txt.Text)) - ((Integer.Parse(discountp_txt.Text) / 100) * (Integer.Parse(cost_txt.Text)))) * (Integer.Parse(qtty_txt.Text))

        DataGridView1.Rows.Add(New String() {command.ExecuteScalar.ToString, Item_txt.Text, qtty_txt.Text, cost_txt.Text, discountp_txt.Text, total.ToString})

        Dim command1 As SqlCommand = New SqlCommand("InsertIntoSuppdet", conx)
        command1.CommandType = CommandType.StoredProcedure
        command1.Parameters.AddWithValue("@qtty", qtty_txt.Text)
        command1.Parameters.AddWithValue("@discount", discountp_txt.Text)
        command1.Parameters.AddWithValue("@item_ID", ID.ToString)
        command1.Parameters.AddWithValue("@cost", cost_txt.Text)
        command1.ExecuteScalar()
        total_txt.Text = Integer.Parse(total_txt.Text) + total
        conx.Close()
        Item_txt.Clear()
        qtty_txt.Text = 0
        max_lbl.Text = " "
        cost_txt.Clear()
        discountp_txt.Text = 0

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim conx As SqlConnection

        conx = conSQL()
        conx.Open()
        

        Dim command As SqlCommand = New SqlCommand("InsertIntoSupplies", conx)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@date", Date_txt.Text)
        command.Parameters.AddWithValue("@supplier_ID", ID_lbl.Text)
        command.Parameters.AddWithValue("@cost", Double.Parse(total_txt.Text))
        command.ExecuteNonQuery()


        Dim x As Integer = 0
        For Each row As DataGridViewRow In DataGridView1.Rows
            Dim command1 As SqlCommand = New SqlCommand("updateSuppdet", conx)
            command1.CommandType = CommandType.StoredProcedure
            command1.Parameters.AddWithValue("@id", DataGridView1.Item(0, x).Value.ToString)
            command1.Parameters.AddWithValue("@sup_ID", ID_txt.Text)
            command1.ExecuteNonQuery()
            x = x + 1

        Next
        conx.Close()
        Me.New_supplier_list_Load(sender, e)
        ID_lbl.Text = " "
        address_txt.Clear()
        Item_txt.Clear()
        qtty_txt.Clear()
        max_lbl.Text = " "
        cost_txt.Clear()
        discountp_txt.Text = 0
        total_txt.Text = 0

        DataGridView1.Rows.Clear()

    End Sub
 
    Private Sub DataGridView1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridView1.MouseUp

        rowdel = DataGridView1.CurrentCell.RowIndex
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()

        Dim id As Integer
        id = DataGridView1.Item(0, rowdel).Value
        total_txt.Text = Integer.Parse(total_txt.Text) - Integer.Parse(DataGridView1.Item(5, rowdel).Value)
        DataGridView1.Rows.RemoveAt(rowdel)
        Dim command1 As SqlCommand = New SqlCommand(" DELETE FROM Supp_det WHERE Supp_det_ID='" + id.ToString + "'", conx)
        command1.CommandType = CommandType.Text
        command1.ExecuteNonQuery()
        conx.Close()
        

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim x As Integer = 0
        For Each row As DataGridViewRow In DataGridView1.Rows
            Dim command1 As SqlCommand = New SqlCommand(" DELETE FROM Supp_det WHERE Supp_det_ID='" + DataGridView1.Item(0, rowdel).Value.ToString + "'", conx)
            command1.CommandType = CommandType.Text
            command1.ExecuteNonQuery()
        Next
       
        conx.Close()
        Me.New_supplier_list_Load(sender, e)
        ID_lbl.Text = " "
        address_txt.Clear()
        Item_txt.Clear()
        qtty_txt.Clear()
        max_lbl.Text = " "
        cost_txt.Clear()
        discountp_txt.Text = 0
        total_txt.Text = 0

        DataGridView1.Rows.Clear()
    End Sub
End Class