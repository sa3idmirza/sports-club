﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class New_Employee
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.address_txt = New System.Windows.Forms.TextBox()
        Me.salary_txt = New System.Windows.Forms.TextBox()
        Me.cell_txt = New System.Windows.Forms.TextBox()
        Me.lname_txt = New System.Windows.Forms.TextBox()
        Me.name_txt = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Statuscombo = New System.Windows.Forms.ComboBox()
        Me.StatusempBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sports_clubDataSet13 = New Sports_Club.Sports_clubDataSet13()
        Me.Stat_lbl = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ID_lbl = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Status_empTableAdapter = New Sports_Club.Sports_clubDataSet13TableAdapters.Status_empTableAdapter()
        CType(Me.StatusempBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sports_clubDataSet13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'address_txt
        '
        Me.address_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.address_txt.Location = New System.Drawing.Point(167, 202)
        Me.address_txt.Name = "address_txt"
        Me.address_txt.Size = New System.Drawing.Size(244, 24)
        Me.address_txt.TabIndex = 41
        Me.address_txt.Text = "Address"
        '
        'salary_txt
        '
        Me.salary_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.salary_txt.Location = New System.Drawing.Point(167, 246)
        Me.salary_txt.Name = "salary_txt"
        Me.salary_txt.Size = New System.Drawing.Size(155, 24)
        Me.salary_txt.TabIndex = 39
        Me.salary_txt.Text = "Salary"
        '
        'cell_txt
        '
        Me.cell_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cell_txt.Location = New System.Drawing.Point(167, 158)
        Me.cell_txt.Name = "cell_txt"
        Me.cell_txt.Size = New System.Drawing.Size(155, 24)
        Me.cell_txt.TabIndex = 34
        Me.cell_txt.Text = "cell"
        '
        'lname_txt
        '
        Me.lname_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lname_txt.Location = New System.Drawing.Point(167, 109)
        Me.lname_txt.Name = "lname_txt"
        Me.lname_txt.Size = New System.Drawing.Size(155, 24)
        Me.lname_txt.TabIndex = 31
        Me.lname_txt.Text = "last name"
        '
        'name_txt
        '
        Me.name_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.name_txt.Location = New System.Drawing.Point(167, 61)
        Me.name_txt.Name = "name_txt"
        Me.name_txt.Size = New System.Drawing.Size(155, 24)
        Me.name_txt.TabIndex = 30
        Me.name_txt.Text = "name"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.MistyRose
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(74, 202)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(62, 18)
        Me.Label6.TabIndex = 40
        Me.Label6.Text = "Address"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.MistyRose
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(79, 246)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(57, 18)
        Me.Label4.TabIndex = 38
        Me.Label4.Text = "Salary :"
        '
        'Statuscombo
        '
        Me.Statuscombo.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.StatusempBindingSource, "Stat_ID", True))
        Me.Statuscombo.DataSource = Me.StatusempBindingSource
        Me.Statuscombo.DisplayMember = "Stat_desc"
        Me.Statuscombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Statuscombo.FormattingEnabled = True
        Me.Statuscombo.Location = New System.Drawing.Point(167, 293)
        Me.Statuscombo.Name = "Statuscombo"
        Me.Statuscombo.Size = New System.Drawing.Size(155, 26)
        Me.Statuscombo.TabIndex = 37
        Me.Statuscombo.ValueMember = "Stat_ID"
        '
        'StatusempBindingSource
        '
        Me.StatusempBindingSource.DataMember = "Status_emp"
        Me.StatusempBindingSource.DataSource = Me.Sports_clubDataSet13
        '
        'Sports_clubDataSet13
        '
        Me.Sports_clubDataSet13.DataSetName = "Sports_clubDataSet13"
        Me.Sports_clubDataSet13.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Stat_lbl
        '
        Me.Stat_lbl.AutoSize = True
        Me.Stat_lbl.BackColor = System.Drawing.Color.MistyRose
        Me.Stat_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Stat_lbl.Location = New System.Drawing.Point(78, 293)
        Me.Stat_lbl.Name = "Stat_lbl"
        Me.Stat_lbl.Size = New System.Drawing.Size(58, 18)
        Me.Stat_lbl.TabIndex = 36
        Me.Stat_lbl.Text = "Status :"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.PaleTurquoise
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(274, 325)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(97, 32)
        Me.Button1.TabIndex = 35
        Me.Button1.Text = "SAVE"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.MistyRose
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(76, 158)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 18)
        Me.Label5.TabIndex = 33
        Me.Label5.Text = "Cell phone :"
        '
        'ID_lbl
        '
        Me.ID_lbl.AutoSize = True
        Me.ID_lbl.BackColor = System.Drawing.Color.Salmon
        Me.ID_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ID_lbl.Location = New System.Drawing.Point(164, 21)
        Me.ID_lbl.Name = "ID_lbl"
        Me.ID_lbl.Size = New System.Drawing.Size(16, 18)
        Me.ID_lbl.TabIndex = 32
        Me.ID_lbl.Text = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.MistyRose
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(78, 112)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 18)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "Last name :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.MistyRose
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(78, 61)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 18)
        Me.Label2.TabIndex = 28
        Me.Label2.Text = "Name :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.MistyRose
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(76, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 18)
        Me.Label1.TabIndex = 27
        Me.Label1.Text = "ID  :"
        '
        'Status_empTableAdapter
        '
        Me.Status_empTableAdapter.ClearBeforeFill = True
        '
        'New_Employee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(484, 379)
        Me.Controls.Add(Me.address_txt)
        Me.Controls.Add(Me.salary_txt)
        Me.Controls.Add(Me.cell_txt)
        Me.Controls.Add(Me.lname_txt)
        Me.Controls.Add(Me.name_txt)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Statuscombo)
        Me.Controls.Add(Me.Stat_lbl)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.ID_lbl)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "New_Employee"
        Me.Text = "New Employee"
        CType(Me.StatusempBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sports_clubDataSet13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents address_txt As System.Windows.Forms.TextBox
    Friend WithEvents salary_txt As System.Windows.Forms.TextBox
    Friend WithEvents cell_txt As System.Windows.Forms.TextBox
    Friend WithEvents lname_txt As System.Windows.Forms.TextBox
    Friend WithEvents name_txt As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Statuscombo As System.Windows.Forms.ComboBox
    Friend WithEvents Stat_lbl As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ID_lbl As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Sports_clubDataSet13 As Sports_Club.Sports_clubDataSet13
    Friend WithEvents StatusempBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Status_empTableAdapter As Sports_Club.Sports_clubDataSet13TableAdapters.Status_empTableAdapter
End Class
