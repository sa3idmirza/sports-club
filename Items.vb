﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class Items

    Private Sub Items_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim ta As DataTable
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim strquery As String
        If (Main.adminis) Then
            strquery = "select Item_ID as ID , Item_desc as Name , Item_qtty as Quantity , Item_price as Price , Item_cost as Cost  from Items"
            Button2.Enabled = True
            Button1.Enabled = True
            Button3.Enabled = True
            Button4.Enabled = True
        Else
            strquery = "select Item_ID as ID , Item_desc as Name , Item_qtty as Quantity , Item_price as Price  from Items"
            Button2.Enabled = False
            Button1.Enabled = False
            Button3.Enabled = False
            Button4.Enabled = False

        End If
        Dim command As SqlCommand = New SqlCommand("Select MAX(Item_ID) + 1 from Items", conx)
        command.CommandType = CommandType.Text
        ID_lbl.Text = command.ExecuteScalar.ToString

        da = New SqlDataAdapter(strquery, conx)
        da.Fill(ds, "Customer_member")
        ta = ds.Tables(0)
        DataGridView1.DataSource = ta

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        If name_txt.TextLength = 0 Or qtty_txt.TextLength = 0 Or IsNumeric(qtty_txt.Text) = False Or price_txt.TextLength = 0 Or IsNumeric(price_txt.Text) = False Then
            MsgBox("fill the required fields correctly !!")
            ErrorProvider1.SetError(name_txt, "This field is a required field")
            ErrorProvider1.SetError(qtty_txt, "This field should be of type integer")
            ErrorProvider1.SetError(price_txt, "This field should be of type integer")
            name_txt.Focus()

        Else
            ErrorProvider1.Clear()

            Dim conx As SqlConnection
            conx = conSQL()

            conx.Open()
            Try
                Dim command As SqlCommand = New SqlCommand("InsertIntoItm", conx)
                command.CommandType = CommandType.StoredProcedure
                command.Parameters.AddWithValue("@name", name_txt.Text)
                command.Parameters.AddWithValue("@price", price_txt.Text)
                command.Parameters.AddWithValue("@qtty", qtty_txt.Text)
                command.ExecuteNonQuery()
                Me.Items_Load(sender, e)
                name_txt.Clear()
                price_txt.Clear()
                name_txt.Select()
                price_txt.Clear()
                qtty_txt.Clear()
            Catch ex As Exception
                MsgBox(ex.Message)
                Throw
            Finally
                conx.Close()
            End Try
        End If
    End Sub
    Private Sub DataGridView1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridView1.MouseUp
        Dim x As Integer
        Try
            x = DataGridView1.CurrentCell.RowIndex
            ID_lbl.Text = x.ToString
            fillfield(x)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub fillfield(ByVal i As Integer)
        Try
            Dim r As DataRow
            Dim da As New SqlDataAdapter
            Dim ds As New DataSet
            Dim t As DataTable
            Dim conx As SqlConnection
            conx = conSQL()
            conx.Open()
            Dim strquery As String
            strquery = "select * from Items "
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Items")
            t = ds.Tables(0)

            r = t.Rows(i)
            ID_lbl.Text = r.Item("Item_ID")
            name_txt.Text = r.Item("Item_desc")
            price_txt.Text = r.Item("Item_price")
            qtty_txt.Text = r.Item("Item_qtty")

            conx.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Items_Load(sender, e)
        name_txt.Clear()
        price_txt.Clear()
        name_txt.Select()
        price_txt.Clear()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        name_txt.Clear()
        price_txt.Clear()
        name_txt.Select()
        price_txt.Clear()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If name_txt.TextLength = 0 Or qtty_txt.TextLength = 0 Or IsNumeric(qtty_txt.Text) = False Or price_txt.TextLength = 0 Or IsNumeric(price_txt.Text) = False Then
            MsgBox("fill the required fields correctly !!")
            ErrorProvider1.SetError(name_txt, "This field is a required field")
            ErrorProvider1.SetError(qtty_txt, "This field should be of type integer")
            ErrorProvider1.SetError(price_txt, "This field should be of type integer")
            name_txt.Focus()

        Else
            ErrorProvider1.Clear()

            Dim conx As SqlConnection
            conx = conSQL()

            conx.Open()

            Dim command As SqlCommand = New SqlCommand("updateitem", conx)
            command.CommandType = CommandType.StoredProcedure
            command.Parameters.AddWithValue("@ID", Integer.Parse(ID_lbl.Text))
            command.Parameters.AddWithValue("@Item_desc", name_txt.Text)

            command.Parameters.AddWithValue("@Item_qtty", Integer.Parse(qtty_txt.Text))
            command.Parameters.AddWithValue("@Item_price", Integer.Parse(price_txt.Text))
            command.ExecuteNonQuery()

            Me.Items_Load(sender, e)
            conx.Close()

        End If
    End Sub
End Class