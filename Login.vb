﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class Login
    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        If (username_txt.Text <> "" And password_txt.Text <> "") Then
            
            If (SQLCon.checkLogin(username_txt.Text, password_txt.Text)) Then
                Main.MenuStrip1.Enabled = True
                Main.Text = "The Club , " + SQLCon.getnamelastname(username_txt.Text) + " , " + Date.Today.Date
                SQLCon.getID(username_txt.Text)
              
                If (isAdmin(username_txt.Text) = False) Then
                    Main.BalanceReport.Enabled = False
                    Main.PayrollReport.Enabled = False

                    Main.SuppReport.Enabled = False
                    Main.ViewSupplier.Enabled = False
                    Main.ViewSuplliesList.Enabled = False
                    Main.paymentvouchermenu.Enabled = False
                    Main.EmployeeItemMenu.Enabled = False
                    Main.CustStat.Enabled = False
                    Main.adminis = False
                    Main.Newsupplist.Enabled = False

                Else
                    Main.BalanceReport.Enabled = True
                    Main.PayrollReport.Enabled = True

                    Main.SuppReport.Enabled = True
                    Main.ViewSupplier.Enabled = True
                    Main.ViewSuplliesList.Enabled = True
                    Main.paymentvouchermenu.Enabled = True
                    Main.EmployeeItemMenu.Enabled = True
                    Main.adminis = True
                    Main.CustStat.Enabled = True
                    Main.Newsupplist.Enabled = True
                End If
                Me.Close()
            Else
                MsgBox("Wrong Username or Password , please check spelling or capslock")
                username_txt.Text = ""
                password_txt.Text = ""
                username_txt.Select()


            End If
        Else
            MsgBox("Enter a username and a password please!")
            username_txt.Text = ""
            password_txt.Text = ""
        End If


    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click

        Main.LoginMenu.Enabled = True
        Me.Close()
    End Sub



    Private Sub Login_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Main.MenuStrip1.Enabled = False


    End Sub
End Class
