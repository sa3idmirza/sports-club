﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class View_Employee
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.emp_combo = New System.Windows.Forms.ComboBox()
        Me.StatusempBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sports_clubDataSet6 = New Sports_Club.Sports_clubDataSet6()
        Me.count_lbl = New System.Windows.Forms.Label()
        Me.Status_empTableAdapter = New Sports_Club.Sports_clubDataSet6TableAdapters.Status_empTableAdapter()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.DataGridView6 = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.address_txt = New System.Windows.Forms.TextBox()
        Me.salary_txt = New System.Windows.Forms.TextBox()
        Me.cell_txt = New System.Windows.Forms.TextBox()
        Me.lname_txt = New System.Windows.Forms.TextBox()
        Me.name_txt = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Statuscombo = New System.Windows.Forms.ComboBox()
        Me.StatusempBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sports_clubDataSet7 = New Sports_Club.Sports_clubDataSet7()
        Me.Stat_lbl = New System.Windows.Forms.Label()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ID_lbl = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.DataGridView5 = New System.Windows.Forms.DataGridView()
        Me.transaction_client_tab = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.DataGridView4 = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.DataGridView3 = New System.Windows.Forms.DataGridView()
        Me.general_client_tab = New System.Windows.Forms.TabPage()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.Status_empTableAdapter1 = New Sports_Club.Sports_clubDataSet7TableAdapters.Status_empTableAdapter()
        CType(Me.StatusempBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sports_clubDataSet6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.DataGridView6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.StatusempBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sports_clubDataSet7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        CType(Me.DataGridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.transaction_client_tab.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.general_client_tab.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.SuspendLayout()
        '
        'emp_combo
        '
        Me.emp_combo.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.StatusempBindingSource, "Stat_ID", True))
        Me.emp_combo.DataSource = Me.StatusempBindingSource
        Me.emp_combo.DisplayMember = "Stat_desc"
        Me.emp_combo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.emp_combo.FormattingEnabled = True
        Me.emp_combo.Location = New System.Drawing.Point(273, 12)
        Me.emp_combo.Name = "emp_combo"
        Me.emp_combo.Size = New System.Drawing.Size(249, 28)
        Me.emp_combo.TabIndex = 14
        Me.emp_combo.ValueMember = "Stat_ID"
        '
        'StatusempBindingSource
        '
        Me.StatusempBindingSource.DataMember = "Status_emp"
        Me.StatusempBindingSource.DataSource = Me.Sports_clubDataSet6
        '
        'Sports_clubDataSet6
        '
        Me.Sports_clubDataSet6.DataSetName = "Sports_clubDataSet6"
        Me.Sports_clubDataSet6.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'count_lbl
        '
        Me.count_lbl.AutoSize = True
        Me.count_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.count_lbl.Location = New System.Drawing.Point(117, 15)
        Me.count_lbl.Name = "count_lbl"
        Me.count_lbl.Size = New System.Drawing.Size(73, 20)
        Me.count_lbl.TabIndex = 13
        Me.count_lbl.Text = "count_lbl"
        '
        'Status_empTableAdapter
        '
        Me.Status_empTableAdapter.ClearBeforeFill = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.DataGridView6)
        Me.TabPage3.Location = New System.Drawing.Point(4, 27)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(989, 386)
        Me.TabPage3.TabIndex = 4
        Me.TabPage3.Text = "Paychecks history"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'DataGridView6
        '
        Me.DataGridView6.AllowUserToAddRows = False
        Me.DataGridView6.AllowUserToDeleteRows = False
        Me.DataGridView6.AllowUserToResizeColumns = False
        Me.DataGridView6.AllowUserToResizeRows = False
        DataGridViewCellStyle1.NullValue = "-"
        Me.DataGridView6.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.DataGridView6.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView6.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView6.GridColor = System.Drawing.SystemColors.ControlLight
        Me.DataGridView6.Location = New System.Drawing.Point(1, 5)
        Me.DataGridView6.MultiSelect = False
        Me.DataGridView6.Name = "DataGridView6"
        Me.DataGridView6.ReadOnly = True
        Me.DataGridView6.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.DataGridView6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView6.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView6.Size = New System.Drawing.Size(986, 377)
        Me.DataGridView6.TabIndex = 2
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage2.Controls.Add(Me.address_txt)
        Me.TabPage2.Controls.Add(Me.salary_txt)
        Me.TabPage2.Controls.Add(Me.cell_txt)
        Me.TabPage2.Controls.Add(Me.lname_txt)
        Me.TabPage2.Controls.Add(Me.name_txt)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.Statuscombo)
        Me.TabPage2.Controls.Add(Me.Stat_lbl)
        Me.TabPage2.Controls.Add(Me.DataGridView2)
        Me.TabPage2.Controls.Add(Me.Button2)
        Me.TabPage2.Controls.Add(Me.Button1)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.ID_lbl)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 27)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(989, 386)
        Me.TabPage2.TabIndex = 3
        Me.TabPage2.Text = "Other Info"
        '
        'address_txt
        '
        Me.address_txt.Location = New System.Drawing.Point(125, 207)
        Me.address_txt.Name = "address_txt"
        Me.address_txt.Size = New System.Drawing.Size(244, 24)
        Me.address_txt.TabIndex = 26
        Me.address_txt.Text = "Address"
        '
        'salary_txt
        '
        Me.salary_txt.Location = New System.Drawing.Point(125, 251)
        Me.salary_txt.Name = "salary_txt"
        Me.salary_txt.Size = New System.Drawing.Size(155, 24)
        Me.salary_txt.TabIndex = 24
        Me.salary_txt.Text = "Salary"
        '
        'cell_txt
        '
        Me.cell_txt.Location = New System.Drawing.Point(125, 163)
        Me.cell_txt.Name = "cell_txt"
        Me.cell_txt.Size = New System.Drawing.Size(155, 24)
        Me.cell_txt.TabIndex = 10
        Me.cell_txt.Text = "cell"
        '
        'lname_txt
        '
        Me.lname_txt.Location = New System.Drawing.Point(125, 114)
        Me.lname_txt.Name = "lname_txt"
        Me.lname_txt.Size = New System.Drawing.Size(155, 24)
        Me.lname_txt.TabIndex = 6
        Me.lname_txt.Text = "last name"
        '
        'name_txt
        '
        Me.name_txt.Location = New System.Drawing.Point(125, 66)
        Me.name_txt.Name = "name_txt"
        Me.name_txt.Size = New System.Drawing.Size(155, 24)
        Me.name_txt.TabIndex = 5
        Me.name_txt.Text = "name"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.MistyRose
        Me.Label6.Location = New System.Drawing.Point(32, 207)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(62, 18)
        Me.Label6.TabIndex = 25
        Me.Label6.Text = "Address"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.MistyRose
        Me.Label4.Location = New System.Drawing.Point(37, 251)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(57, 18)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Salary :"
        '
        'Statuscombo
        '
        Me.Statuscombo.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.StatusempBindingSource1, "Stat_ID", True))
        Me.Statuscombo.DataSource = Me.StatusempBindingSource1
        Me.Statuscombo.DisplayMember = "Stat_desc"
        Me.Statuscombo.FormattingEnabled = True
        Me.Statuscombo.Location = New System.Drawing.Point(125, 298)
        Me.Statuscombo.Name = "Statuscombo"
        Me.Statuscombo.Size = New System.Drawing.Size(155, 26)
        Me.Statuscombo.TabIndex = 22
        Me.Statuscombo.ValueMember = "Stat_ID"
        '
        'StatusempBindingSource1
        '
        Me.StatusempBindingSource1.DataMember = "Status_emp"
        Me.StatusempBindingSource1.DataSource = Me.Sports_clubDataSet7
        '
        'Sports_clubDataSet7
        '
        Me.Sports_clubDataSet7.DataSetName = "Sports_clubDataSet7"
        Me.Sports_clubDataSet7.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Stat_lbl
        '
        Me.Stat_lbl.AutoSize = True
        Me.Stat_lbl.BackColor = System.Drawing.Color.MistyRose
        Me.Stat_lbl.Location = New System.Drawing.Point(36, 298)
        Me.Stat_lbl.Name = "Stat_lbl"
        Me.Stat_lbl.Size = New System.Drawing.Size(58, 18)
        Me.Stat_lbl.TabIndex = 21
        Me.Stat_lbl.Text = "Status :"
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.AllowUserToResizeColumns = False
        Me.DataGridView2.AllowUserToResizeRows = False
        DataGridViewCellStyle2.NullValue = "-"
        Me.DataGridView2.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle2
        Me.DataGridView2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView2.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.GridColor = System.Drawing.SystemColors.ControlLight
        Me.DataGridView2.Location = New System.Drawing.Point(375, 26)
        Me.DataGridView2.MultiSelect = False
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.DataGridView2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView2.Size = New System.Drawing.Size(589, 240)
        Me.DataGridView2.StandardTab = True
        Me.DataGridView2.TabIndex = 20
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.PaleTurquoise
        Me.Button2.ForeColor = System.Drawing.Color.Black
        Me.Button2.Location = New System.Drawing.Point(545, 330)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(97, 32)
        Me.Button2.TabIndex = 18
        Me.Button2.Text = "Add New"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.PaleTurquoise
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(232, 330)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(97, 32)
        Me.Button1.TabIndex = 17
        Me.Button1.Text = "update"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.MistyRose
        Me.Label5.Location = New System.Drawing.Point(34, 163)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 18)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Cell phone :"
        '
        'ID_lbl
        '
        Me.ID_lbl.AutoSize = True
        Me.ID_lbl.BackColor = System.Drawing.Color.Salmon
        Me.ID_lbl.Location = New System.Drawing.Point(122, 26)
        Me.ID_lbl.Name = "ID_lbl"
        Me.ID_lbl.Size = New System.Drawing.Size(16, 18)
        Me.ID_lbl.TabIndex = 8
        Me.ID_lbl.Text = "0"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.MistyRose
        Me.Label3.Location = New System.Drawing.Point(36, 117)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(85, 18)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Last name :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.MistyRose
        Me.Label2.Location = New System.Drawing.Point(36, 66)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 18)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Name :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.MistyRose
        Me.Label1.Location = New System.Drawing.Point(34, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(34, 18)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "ID  :"
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.DataGridView5)
        Me.TabPage1.Location = New System.Drawing.Point(4, 27)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Size = New System.Drawing.Size(989, 386)
        Me.TabPage1.TabIndex = 2
        Me.TabPage1.Text = "Reservations"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'DataGridView5
        '
        Me.DataGridView5.AllowUserToAddRows = False
        Me.DataGridView5.AllowUserToDeleteRows = False
        Me.DataGridView5.AllowUserToResizeColumns = False
        Me.DataGridView5.AllowUserToResizeRows = False
        DataGridViewCellStyle3.NullValue = "-"
        Me.DataGridView5.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle3
        Me.DataGridView5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView5.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView5.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView5.GridColor = System.Drawing.SystemColors.ControlLight
        Me.DataGridView5.Location = New System.Drawing.Point(2, 4)
        Me.DataGridView5.MultiSelect = False
        Me.DataGridView5.Name = "DataGridView5"
        Me.DataGridView5.ReadOnly = True
        Me.DataGridView5.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.DataGridView5.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView5.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView5.Size = New System.Drawing.Size(986, 377)
        Me.DataGridView5.TabIndex = 1
        '
        'transaction_client_tab
        '
        Me.transaction_client_tab.Controls.Add(Me.GroupBox2)
        Me.transaction_client_tab.Controls.Add(Me.GroupBox1)
        Me.transaction_client_tab.Location = New System.Drawing.Point(4, 27)
        Me.transaction_client_tab.Name = "transaction_client_tab"
        Me.transaction_client_tab.Padding = New System.Windows.Forms.Padding(3)
        Me.transaction_client_tab.Size = New System.Drawing.Size(989, 386)
        Me.transaction_client_tab.TabIndex = 1
        Me.transaction_client_tab.Text = "Transaction"
        Me.transaction_client_tab.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.DataGridView4)
        Me.GroupBox2.Location = New System.Drawing.Point(47, 205)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(818, 189)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Receipts"
        '
        'DataGridView4
        '
        Me.DataGridView4.AllowUserToAddRows = False
        Me.DataGridView4.AllowUserToDeleteRows = False
        Me.DataGridView4.AllowUserToResizeColumns = False
        Me.DataGridView4.AllowUserToResizeRows = False
        DataGridViewCellStyle4.NullValue = "-"
        Me.DataGridView4.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridView4.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView4.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView4.GridColor = System.Drawing.SystemColors.ControlLight
        Me.DataGridView4.Location = New System.Drawing.Point(13, 22)
        Me.DataGridView4.MultiSelect = False
        Me.DataGridView4.Name = "DataGridView4"
        Me.DataGridView4.ReadOnly = True
        Me.DataGridView4.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.DataGridView4.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView4.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView4.Size = New System.Drawing.Size(787, 144)
        Me.DataGridView4.TabIndex = 2
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.DataGridView3)
        Me.GroupBox1.Location = New System.Drawing.Point(47, 10)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(818, 195)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Invoices"
        '
        'DataGridView3
        '
        Me.DataGridView3.AllowUserToAddRows = False
        Me.DataGridView3.AllowUserToDeleteRows = False
        Me.DataGridView3.AllowUserToResizeColumns = False
        Me.DataGridView3.AllowUserToResizeRows = False
        DataGridViewCellStyle5.NullValue = "-"
        Me.DataGridView3.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridView3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView3.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView3.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView3.GridColor = System.Drawing.SystemColors.ControlLight
        Me.DataGridView3.Location = New System.Drawing.Point(17, 23)
        Me.DataGridView3.MultiSelect = False
        Me.DataGridView3.Name = "DataGridView3"
        Me.DataGridView3.ReadOnly = True
        Me.DataGridView3.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.DataGridView3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView3.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView3.Size = New System.Drawing.Size(783, 154)
        Me.DataGridView3.TabIndex = 1
        '
        'general_client_tab
        '
        Me.general_client_tab.Controls.Add(Me.DataGridView1)
        Me.general_client_tab.Location = New System.Drawing.Point(4, 27)
        Me.general_client_tab.Name = "general_client_tab"
        Me.general_client_tab.Padding = New System.Windows.Forms.Padding(3)
        Me.general_client_tab.Size = New System.Drawing.Size(989, 386)
        Me.general_client_tab.TabIndex = 0
        Me.general_client_tab.Text = "General"
        Me.general_client_tab.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToResizeColumns = False
        Me.DataGridView1.AllowUserToResizeRows = False
        DataGridViewCellStyle6.NullValue = "-"
        Me.DataGridView1.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.BackgroundColor = System.Drawing.SystemColors.Control
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.GridColor = System.Drawing.SystemColors.ControlLight
        Me.DataGridView1.Location = New System.Drawing.Point(-2, 3)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.DataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.Size = New System.Drawing.Size(993, 380)
        Me.DataGridView1.TabIndex = 0
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.general_client_tab)
        Me.TabControl1.Controls.Add(Me.transaction_client_tab)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControl1.Location = New System.Drawing.Point(1, 46)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(997, 417)
        Me.TabControl1.TabIndex = 12
        '
        'Status_empTableAdapter1
        '
        Me.Status_empTableAdapter1.ClearBeforeFill = True
        '
        'View_Employee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(995, 447)
        Me.Controls.Add(Me.emp_combo)
        Me.Controls.Add(Me.count_lbl)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "View_Employee"
        Me.Text = "View Employee"
        CType(Me.StatusempBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sports_clubDataSet6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.DataGridView6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.StatusempBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sports_clubDataSet7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        CType(Me.DataGridView5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.transaction_client_tab.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.general_client_tab.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents emp_combo As System.Windows.Forms.ComboBox
    Friend WithEvents count_lbl As System.Windows.Forms.Label
    Friend WithEvents Sports_clubDataSet6 As Sports_Club.Sports_clubDataSet6
    Friend WithEvents StatusempBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Status_empTableAdapter As Sports_Club.Sports_clubDataSet6TableAdapters.Status_empTableAdapter
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents DataGridView6 As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents address_txt As System.Windows.Forms.TextBox
    Friend WithEvents salary_txt As System.Windows.Forms.TextBox
    Friend WithEvents cell_txt As System.Windows.Forms.TextBox
    Friend WithEvents lname_txt As System.Windows.Forms.TextBox
    Friend WithEvents name_txt As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Statuscombo As System.Windows.Forms.ComboBox
    Friend WithEvents Stat_lbl As System.Windows.Forms.Label
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ID_lbl As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents DataGridView5 As System.Windows.Forms.DataGridView
    Friend WithEvents transaction_client_tab As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridView4 As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridView3 As System.Windows.Forms.DataGridView
    Friend WithEvents general_client_tab As System.Windows.Forms.TabPage
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents Sports_clubDataSet7 As Sports_Club.Sports_clubDataSet7
    Friend WithEvents StatusempBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents Status_empTableAdapter1 As Sports_Club.Sports_clubDataSet7TableAdapters.Status_empTableAdapter
End Class
