﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class Payment_Voucher

    Private Sub Payment_Voucher_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()

        Dim command As SqlCommand = New SqlCommand("Select MAX(Voucher_ID) + 1 from Payment_voucher", conx)
        command.CommandType = CommandType.Text

        ID_txt.Text = command.ExecuteScalar.ToString

        Date_txt.Text = Date.Now.Month.ToString + "/" + Date.Now.Day.ToString + "/" + Date.Now.Year.ToString
    End Sub

    Private Sub name_txt_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles name_txt.MouseDoubleClick
        Search.MdiParent = Main
        Search.Show()
        Search.str = "select Supplier_ID As 'ID' , Supplier_name as 'Name' , Supplier_cell as 'Cell Phone'  , Supplier_add as 'Address' from Suppliers where Supplier_name like '%"

        Search.fromsupppay = True
    End Sub

    Private Sub name_txt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles name_txt.TextChanged
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Salary_txt.Text = 0

        Dim sqlcommand4 As New SqlCommand("Select Count(Supplies_ID) from Supplies where Supplier_ID = '" + ID_lbl.Text + "'", conx)
        sqlcommand4.CommandType = CommandType.Text
        Dim sqlcommand3 As New SqlCommand("Select SUM(Supplies_cost) from Supplies where Supplier_ID = '" + ID_lbl.Text + "'", conx)
        sqlcommand3.CommandType = CommandType.Text



        Dim sqlcommand1 As New SqlCommand("Select SUM(Voucher_amount) from Payment_voucher where Supplier_ID = '" + ID_lbl.Text + "'", conx)
        sqlcommand1.CommandType = CommandType.Text
        Dim sqlcommand2 As New SqlCommand("Select Count(Voucher_ID) from Payment_voucher where Supplier_ID = '" + ID_lbl.Text + "'", conx)
        sqlcommand2.CommandType = CommandType.Text


        Dim pay, cost As Integer

        If (Integer.Parse(sqlcommand4.ExecuteScalar.ToString) = 0) Then
            pay = 0
        Else
            pay = Integer.Parse(sqlcommand3.ExecuteScalar.ToString)
        End If
        If (Integer.Parse(sqlcommand2.ExecuteScalar.ToString) = 0) Then
            cost = 0
        Else
            cost = Integer.Parse(sqlcommand1.ExecuteScalar.ToString)
        End If
        Salary_txt.Text = (cost - pay).ToString

        conx.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()

        Dim sqlcommand As New SqlCommand("Insert Into Payment_voucher(Voucher_date,Voucher_amount,Supplier_ID) values('" + Date_txt.Text + "' , '" + amount_txt.Text + "','" + ID_lbl.Text + "')", conx)
        sqlcommand.CommandType = CommandType.Text
        sqlcommand.ExecuteNonQuery()
        conx.Close()
        Me.Payment_Voucher_Load(sender, e)
        name_txt.Clear()
        Salary_txt.Clear()
        amount_txt.Clear()


    End Sub
End Class