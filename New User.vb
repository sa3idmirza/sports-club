﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class New_User

    Private Sub New_User_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Sports_clubDataSet12.Login' table. You can move, or remove it, as needed.
        Me.LoginTableAdapter.Fill(Me.Sports_clubDataSet12.Login)

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim conx As SqlConnection
        conx = conSQL()

        conx.Open()
        Try
            Dim command As SqlCommand = New SqlCommand("InsertIntoLogin", conx)
            command.CommandType = CommandType.StoredProcedure

            command.Parameters.AddWithValue("@username", username_txt.Text)
            command.Parameters.AddWithValue("@password", password_txt.Text)
            command.Parameters.AddWithValue("@emp_ID", emp_txt.Text)
            command.ExecuteNonQuery()
            Me.New_User_Load(sender, e)
            username_txt.Clear()
            password_txt.Clear()
            username_txt.Select()
            emp_txt.Clear()
        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try
    End Sub

    Private Sub emp_txt_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles emp_txt.MouseDoubleClick
        Search.MdiParent = Main
        Search.Show()
        Search.str = "select Emp_ID As 'ID' , Emp_name as 'Name' , Emp_lastname as 'Last Name' , salary as 'Salary' , Emp_cell as 'Cell Number' from Employee where Emp_name like '%"
        Search.fromlogin = True
    End Sub

 
    Private Sub emp_txt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles emp_txt.TextChanged

    End Sub
End Class