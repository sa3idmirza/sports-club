﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class Payment_Employee


    Private Sub Payment_Employee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()

        Dim command As SqlCommand = New SqlCommand("Select MAX(Pay_ID) + 1 from Payroll", conx)
        command.CommandType = CommandType.Text
        ID_txt.Text = command.ExecuteScalar.ToString

        Date_txt.Text = Date.Now.Month.ToString + "/" + Date.Now.Day.ToString + "/" + Date.Now.Year.ToString
    End Sub

    Private Sub name_txt_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles name_txt.MouseDoubleClick
        Search.MdiParent = Main
        Search.Show()
        Search.str = "select Emp_ID As 'ID' , Emp_name as 'Name' , Emp_lastname as 'Last Name' , salary as 'Salary' , Emp_cell as 'Cell Number' from Employee where Emp_name like '%"

        Search.fromPaymentEmp = True
    End Sub

    
    Private Sub bonux_txt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bonux_txt.TextChanged
        If (bonux_txt.Text <> "") Then
            Amount_txt.Text = Integer.Parse(bonux_txt.Text) + Integer.Parse(Salary_txt.Text)
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()

        Dim command As SqlCommand = New SqlCommand("InsertIntoPayroll", conx)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@date", Date_txt.Text)
        command.Parameters.AddWithValue("@bonus", bonux_txt.Text)
        command.Parameters.AddWithValue("@amount", Amount_txt.Text)
        command.Parameters.AddWithValue("@emp_ID", ID_lbl.Text)
        command.ExecuteNonQuery()
        conx.Close()

        Salary_txt.Clear()
        bonux_txt.Clear()
        Amount_txt.Clear()
        Me.Payment_Employee_Load(sender, e)
        For Each frm As Form In Application.OpenForms
            If frm.Name.Equals("View_Employee") Then
                View_Employee.emp_combo_SelectedIndexChanged(sender, e)
                View_Employee.Button1_Click(sender, e)
            End If
        Next
    End Sub

    Private Sub name_txt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles name_txt.TextChanged
        
    End Sub
End Class