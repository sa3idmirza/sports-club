﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class New_Employee

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim conx As SqlConnection
     
        conx = conSQL()

        conx.Open()

        Dim command As SqlCommand = New SqlCommand("InsertIntoEmp", conx)
        command.CommandType = CommandType.StoredProcedure


        command.Parameters.AddWithValue("@name", name_txt.Text)
        command.Parameters.AddWithValue("@lastname", lname_txt.Text)
        command.Parameters.AddWithValue("@address", address_txt.Text)
        command.Parameters.AddWithValue("@salary", salary_txt.Text)
        command.Parameters.AddWithValue("@cell", cell_txt.Text)
        command.Parameters.AddWithValue("@stat", Statuscombo.SelectedValue)


        command.ExecuteNonQuery()

        name_txt.Clear()
        lname_txt.Clear()
        cell_txt.Clear()
        address_txt.Clear()
        salary_txt.Clear()

        Me.New_Employee_Load(sender, e)
        For Each frm As Form In Application.OpenForms
            If frm.Name.Equals("View_Employee") Then
                View_Employee.emp_combo_SelectedIndexChanged(sender, e)
                View_Employee.Button1_Click(sender, e)
            End If
        Next



    End Sub

    Private Sub New_Employee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Sports_clubDataSet13.Status_emp' table. You can move, or remove it, as needed.
        Me.Status_empTableAdapter.Fill(Me.Sports_clubDataSet13.Status_emp)
        Dim conx As New SqlConnection
        conx = conSQL()
        conx.Open()
        Try
            Dim cmd As SqlCommand = New SqlCommand("SELECT MAX (Emp_ID) + 1 FROM Employee ", conx)
            cmd.CommandType = CommandType.Text
            ID_lbl.Text = cmd.ExecuteScalar().ToString
        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try
    End Sub
End Class