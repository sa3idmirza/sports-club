﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class New_Reservation
    Dim check As Boolean
    Private Sub New_Reservation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Sports_clubDataSet15.Rental_Item' table. You can move, or remove it, as needed.
        Me.Rental_ItemTableAdapter.Fill(Me.Sports_clubDataSet15.Rental_Item)
        check = False
        Dim conx As SqlConnection
        Act_txt.Enabled = False
        conx = conSQL()
        conx.Open()

        Dim command As SqlCommand = New SqlCommand("Select MAX(Res_ID) + 1 from Reservation", conx)
        command.CommandType = CommandType.Text
        ID_txt.Text = command.ExecuteScalar.ToString

        ID_lbl.Text = Main.user_ID.ToString

        Dim command1 As SqlCommand = New SqlCommand("Select Emp_name from Employee where Emp_ID = '" + ID_lbl.Text + "'", conx)
        command1.CommandType = CommandType.Text
        name_txt.Text = command1.ExecuteScalar.ToString

       

        

    End Sub

    Private Sub Cust_name_txt_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Cust_name_txt.MouseDoubleClick
        Search.MdiParent = Main
        Search.Show()
        Search.str = "select Cust_ID As 'ID' , Cust_name as 'Name' ,Cust_lastname as 'Last Name'   from Customer_member where Cust_name like '%"
        Search.newcust_but.visible = True
        Search.fromrescust = True
    End Sub
   
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim conx As SqlConnection

        conx = conSQL()
        conx.Open()

        Dim command As SqlCommand = New SqlCommand("Select Rent_Price from Rental_Item where Rent_It_ID = '" + item_combo.SelectedValue.ToString + "'", conx)
        command.CommandType = CommandType.Text


        DataGridView1.Rows.Add(New String() {item_combo.SelectedValue.ToString, item_combo.Text, command.ExecuteScalar.ToString})
        conx.Close()
    End Sub
 

   
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim conx As SqlConnection

        conx = conSQL()
        conx.Open()

        Dim command As SqlCommand = New SqlCommand("InsertRes", conx)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@date", DateTimePicker1.Value)
        command.Parameters.AddWithValue("@reshrbeg", DateTimePicker2.Value)
        command.Parameters.AddWithValue("@reshrend", DateTimePicker3.Value)
        command.Parameters.AddWithValue("@cust_ID", Cust_ID_lbl.Text)
        command.Parameters.AddWithValue("@emp_ID", ID_lbl.Text)
        command.Parameters.AddWithValue("@act_ID", Act_txt.Text)

        command.ExecuteNonQuery()

       
        Dim x As Integer = 0
        For Each row As DataGridViewRow In DataGridView1.Rows
            Dim command1 As SqlCommand = New SqlCommand("Insertdet", conx)
            command1.CommandType = CommandType.StoredProcedure
            command1.Parameters.AddWithValue("@res_id", ID_txt.Text)
            command1.Parameters.AddWithValue("@res_rent_it", DataGridView1.Item(0, x).Value)
            command1.ExecuteNonQuery()
            x = x + 1

        Next
        conx.Close()
        Me.New_Reservation_Load(sender, e)
        Cust_ID_lbl.Text = " "
        Act_txt.Clear()
        Cust_name_txt.Clear()
        DataGridView1.Rows.Clear()






    End Sub

    Private Sub Act_txt_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Act_txt.MouseDoubleClick
        Search.MdiParent = Main
        Search.Show()
        Search.str = "exec selectAvailableAct '" + DateTimePicker2.Value + "', '" + DateTimePicker3.Value + "' ,'" + DateTimePicker1.Value + "', '"
        Search.fromresact = True
    End Sub

    

    Private Sub DateTimePicker3_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker3.ValueChanged
         
            Act_txt.Enabled = True

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim x As Integer
        x = DataGridView1.CurrentCell.RowIndex

        DataGridView1.Rows.RemoveAt(x)

    End Sub
End Class