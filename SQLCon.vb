﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Module SQLCon
   
    Function conSQL() As SqlConnection
        Dim connectionString As String = "Data Source=127.0.0.1\SQLEXPRESS;Initial Catalog=Sports club;Integrated Security =true"
        Dim sqlcon As SqlConnection = New SqlConnection(connectionString)
        Return sqlcon
    End Function
    Function checkLogin(ByVal username As String, ByVal password As String) As Boolean
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Try
            Dim getUsername As SqlCommand = New SqlCommand("SelectUsername", conx)
            getUsername.CommandType = CommandType.StoredProcedure
            getUsername.Parameters.AddWithValue("@username", username)
            getUsername.Parameters.AddWithValue("@password", password)
            getUsername.ExecuteNonQuery()
            getUsername.ExecuteScalar()
            Dim getPassword As SqlCommand = New SqlCommand("SelectPassword", conx)
            getPassword.CommandType = CommandType.StoredProcedure
            getPassword.Parameters.AddWithValue("@password", password)
            getPassword.ExecuteNonQuery()
            getPassword.ExecuteScalar()
            If (getUsername.ExecuteScalar() = username And getPassword.ExecuteScalar() = password) Then
                conx.Close()
                Return True
            Else
                conx.Close()
                Return False
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try
    End Function
    Function getnamelastname(ByVal username As String) As String
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim str As String = ""
        Try
            Dim selectnamelname As SqlCommand = New SqlCommand("SelectEmpName", conx)
            selectnamelname.CommandType = CommandType.StoredProcedure
            selectnamelname.Parameters.AddWithValue("@username", username)
            selectnamelname.ExecuteNonQuery()
            str += selectnamelname.ExecuteScalar().ToString + " "

            selectnamelname = New SqlCommand("SelectEmpLastname", conx)
            selectnamelname.CommandType = CommandType.StoredProcedure
            selectnamelname.Parameters.AddWithValue("@username", username)
            selectnamelname.ExecuteNonQuery()
            str += selectnamelname.ExecuteScalar().ToString + " "
            
            Return str
        Catch ex As Exception

            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try
    End Function

    Function getID(ByVal username As String)
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim selectdesc As SqlCommand = New SqlCommand("getUserID", conx)
        selectdesc.CommandType = CommandType.StoredProcedure
        selectdesc.Parameters.AddWithValue("@username", username)
        selectdesc.ExecuteNonQuery()
        Main.user_ID = Integer.Parse(selectdesc.ExecuteScalar())
    End Function
    Function isAdmin(ByVal username As String) As Boolean
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim selectdesc As SqlCommand = New SqlCommand("checkifadmin", conx)
        selectdesc.CommandType = CommandType.StoredProcedure
        selectdesc.Parameters.AddWithValue("@username", username)
        selectdesc.ExecuteNonQuery()
        selectdesc.ExecuteScalar()
        If (selectdesc.ExecuteScalar() = username) Then
            conx.Close()
            Return True
        End If
        conx.Close()
        Return False
    End Function
     
End Module
