﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class View_Employee

     

    Public Sub View_Employee_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Sports_clubDataSet7.Status_emp' table. You can move, or remove it, as needed.
        Me.Status_empTableAdapter1.Fill(Me.Sports_clubDataSet7.Status_emp)
        'TODO: This line of code loads data into the 'Sports_clubDataSet6.Status_emp' table. You can move, or remove it, as needed.
        Me.Status_empTableAdapter.Fill(Me.Sports_clubDataSet6.Status_emp)


        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim strquery As String
        strquery = "select Emp_ID as 'ID' ,Emp_name as 'First Name' ,Emp_lastname as 'Last Name'from Employee "
        da = New SqlDataAdapter(strquery, conx)
        da.Fill(ds, "Employee")
        t = ds.Tables(0)
        DataGridView2.DataSource = t
        Me.emp_combo_SelectedIndexChanged(sender, e)
        conx.Close()
    End Sub
    Public Sub emp_combo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles emp_combo.SelectedIndexChanged
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim strquery As String
        Dim strquery2 As String

        strquery = "select Emp_ID as 'ID',Emp_name as 'First name', Emp_lastname as 'Last name',  salary as 'Salary' from Employee"
        strquery2 = "select Count(Emp_ID) from Employee "
        If (emp_combo.Text <> "ALL") Then
            strquery += " where Status_ID = " + emp_combo.SelectedValue.ToString
            strquery2 += "where Status_ID = '" + emp_combo.SelectedValue.ToString + "'"
        End If
        Try
            Dim cmd As SqlCommand = New SqlCommand(strquery2, conx)
            cmd = New SqlCommand(strquery2, conx)
            cmd.CommandType = CommandType.Text
            count_lbl.Text = "Record Count : " + cmd.ExecuteScalar().ToString
            'fill the datagridview 
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Employee")
            t = ds.Tables(0)
            DataGridView1.DataSource = t
        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()

        End Try

    End Sub


    Public Sub fillfield(ByVal i As Integer)
        Try
            Dim r As DataRow
            Dim da As New SqlDataAdapter
            Dim ds As New DataSet
            Dim t As DataTable
            Dim conx As SqlConnection
            conx = conSQL()
            conx.Open()
            Dim strquery As String
            strquery = "select * from Employee "
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Employee")
            t = ds.Tables(0)

            r = t.Rows(i)
            ID_lbl.Text = r.Item("Emp_ID")
            name_txt.Text = r.Item("Emp_name")
            lname_txt.Text = r.Item("Emp_lastname")
            cell_txt.Text = r.Item("Emp_cell")
            address_txt.Text = r.Item("Emp_address")
            salary_txt.Text = r.Item("salary")
            Statuscombo.SelectedValue = r.Item("Status_ID")
            conx.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub





    Private Sub DataGridView2_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridView2.MouseUp
        Dim x As Integer
        Try
            x = DataGridView2.CurrentCell.RowIndex
            ID_lbl.Text = x.ToString
            fillfield(x)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub



    Private Sub DataGridView1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridView1.MouseUp
        Dim x As Integer
        Try
            x = DataGridView1.CurrentRow.Index

            getID(x)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub getID(ByVal i As Integer)
        Try
            Dim rowindex As Integer = 0
            Dim ID As Integer
            ID = DataGridView1.Item(0, i).Value
            filldatagridview3(ID)
            filldatagridview4(ID)
            filldatagridview5(ID)
            filldatagridview6(ID)
            For Each row As DataGridViewRow In DataGridView2.Rows

                If row.Cells.Item("ID").Value = ID Then
                    Exit For


                End If
                rowindex = rowindex + 1
            Next
            DataGridView2.Rows(rowindex).Selected = True

            fillfield(rowindex)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Function filldatagridview3(ByVal ID As Integer)
        Dim conx As SqlConnection
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable

        conx = conSQL()

        conx.Open()
        Try


            Dim strquery As String
            strquery = "select Inv_ID as 'Reference' , Inv_date as 'Issue Date' , Inv_amount as 'Amount' , Inv_discount as 'Discount %' , total_net as 'Total' from Invoices  where  Emp_ID =  '" + ID.ToString + "'"
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Invoices")
            t = ds.Tables(0)
            DataGridView3.DataSource = t

        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try
    End Function

    Function filldatagridview4(ByVal ID As Integer)
        Dim conx As SqlConnection
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable

        conx = conSQL()

        conx.Open()
        Try


            Dim strquery As String
            strquery = "select Rec_ID as 'Reference' , Rec_date as 'Issue Date' , Rec_amount as 'Amount' from Receipts  where  Emp_ID =  '" + ID.ToString + "'"
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Receipts")
            t = ds.Tables(0)
            DataGridView4.DataSource = t

        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try
    End Function
    Function filldatagridview5(ByVal ID As Integer)
        Dim conx As SqlConnection
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable

        conx = conSQL()

        conx.Open()
        Try


            Dim strquery As String
            strquery = "select Res_ID as 'Reference' , Res_date as 'Reservation Date' , Cancel_flag as 'Canceled' ,Res_hour_beg as 'At Time',Res_hour_end as 'To time'    from Reservation  where  Emp_ID =  '" + ID.ToString + "'"
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Invoices")
            t = ds.Tables(0)
            DataGridView5.DataSource = t

        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try
    End Function
    Function filldatagridview6(ByVal ID As Integer)
        Dim conx As SqlConnection
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable

        conx = conSQL()

        conx.Open()
        Try


            Dim strquery As String
            strquery = "select Pay_ID as 'Reference' , Date as 'Date' , Bonus as 'Bonus' ,Amount_Payed as 'Amount + bonus'     from Payroll  where  Emp_ID =  '" + ID.ToString + "'"
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Invoices")
            t = ds.Tables(0)
            DataGridView6.DataSource = t

        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try
    End Function



    Public Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim conx As SqlConnection
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable

        conx = conSQL()

        conx.Open()

        Dim command As SqlCommand = New SqlCommand("UpdateEmp", conx)
        command.CommandType = CommandType.StoredProcedure

        command.Parameters.AddWithValue("@id", ID_lbl.Text)
        command.Parameters.AddWithValue("@name", name_txt.Text)
        command.Parameters.AddWithValue("@lastname", lname_txt.Text)
        command.Parameters.AddWithValue("@address", address_txt.Text)
        command.Parameters.AddWithValue("@salary", salary_txt.Text)
        command.Parameters.AddWithValue("@cell", cell_txt.Text)
        command.Parameters.AddWithValue("@stat", Statuscombo.SelectedValue)


        command.ExecuteNonQuery()

        Dim strquery As String
        strquery = "select Emp_ID as 'ID' , Emp_name as 'First Name' , Emp_lastname as 'Last Name' from Employee "
        da = New SqlDataAdapter(strquery, conx)
        da.Fill(ds, "Employee")
        t = ds.Tables(0)
        DataGridView2.DataSource = t



    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        New_Employee.MdiParent = Main
        New_Employee.Show()
    End Sub
     
End Class