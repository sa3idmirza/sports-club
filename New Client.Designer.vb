﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class New_Client
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Statuscombo = New System.Windows.Forms.ComboBox()
        Me.StatuscustBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sports_clubDataSet10 = New Sports_Club.Sports_clubDataSet10()
        Me.StatuscustBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Sports_clubDataSet1 = New Sports_Club.Sports_clubDataSet1()
        Me.Stat_lbl = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.cell_txt = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ID_lbl = New System.Windows.Forms.Label()
        Me.lname_txt = New System.Windows.Forms.TextBox()
        Me.name_txt = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Status_custTableAdapter = New Sports_Club.Sports_clubDataSet1TableAdapters.Status_custTableAdapter()
        Me.Status_custTableAdapter1 = New Sports_Club.Sports_clubDataSet10TableAdapters.Status_custTableAdapter()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me.StatuscustBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sports_clubDataSet10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StatuscustBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Sports_clubDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Statuscombo
        '
        Me.Statuscombo.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.StatuscustBindingSource1, "Stat_ID", True))
        Me.Statuscombo.DataSource = Me.StatuscustBindingSource1
        Me.Statuscombo.DisplayMember = "Stat_description"
        Me.Statuscombo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Statuscombo.FormattingEnabled = True
        Me.Statuscombo.Location = New System.Drawing.Point(170, 221)
        Me.Statuscombo.Name = "Statuscombo"
        Me.Statuscombo.Size = New System.Drawing.Size(155, 28)
        Me.Statuscombo.TabIndex = 33
        Me.Statuscombo.ValueMember = "Stat_ID"
        '
        'StatuscustBindingSource1
        '
        Me.StatuscustBindingSource1.DataMember = "Status_cust"
        Me.StatuscustBindingSource1.DataSource = Me.Sports_clubDataSet10
        '
        'Sports_clubDataSet10
        '
        Me.Sports_clubDataSet10.DataSetName = "Sports_clubDataSet10"
        Me.Sports_clubDataSet10.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'StatuscustBindingSource
        '
        Me.StatuscustBindingSource.DataMember = "Status_cust"
        Me.StatuscustBindingSource.DataSource = Me.Sports_clubDataSet1
        '
        'Sports_clubDataSet1
        '
        Me.Sports_clubDataSet1.DataSetName = "Sports_clubDataSet1"
        Me.Sports_clubDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Stat_lbl
        '
        Me.Stat_lbl.AutoSize = True
        Me.Stat_lbl.BackColor = System.Drawing.Color.MistyRose
        Me.Stat_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Stat_lbl.Location = New System.Drawing.Point(79, 224)
        Me.Stat_lbl.Name = "Stat_lbl"
        Me.Stat_lbl.Size = New System.Drawing.Size(64, 20)
        Me.Stat_lbl.TabIndex = 32
        Me.Stat_lbl.Text = "Status :"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.PaleTurquoise
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.Black
        Me.Button1.Location = New System.Drawing.Point(140, 281)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(97, 32)
        Me.Button1.TabIndex = 31
        Me.Button1.Text = "save"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'cell_txt
        '
        Me.cell_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cell_txt.Location = New System.Drawing.Point(170, 163)
        Me.cell_txt.Name = "cell_txt"
        Me.cell_txt.Size = New System.Drawing.Size(155, 26)
        Me.cell_txt.TabIndex = 30
        Me.cell_txt.Text = " "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.MistyRose
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(78, 163)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(92, 20)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "Cell phone :"
        '
        'ID_lbl
        '
        Me.ID_lbl.AutoSize = True
        Me.ID_lbl.BackColor = System.Drawing.Color.Salmon
        Me.ID_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ID_lbl.Location = New System.Drawing.Point(167, 21)
        Me.ID_lbl.Name = "ID_lbl"
        Me.ID_lbl.Size = New System.Drawing.Size(18, 20)
        Me.ID_lbl.TabIndex = 28
        Me.ID_lbl.Text = "0"
        '
        'lname_txt
        '
        Me.lname_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lname_txt.Location = New System.Drawing.Point(170, 111)
        Me.lname_txt.Name = "lname_txt"
        Me.lname_txt.Size = New System.Drawing.Size(155, 26)
        Me.lname_txt.TabIndex = 27
        '
        'name_txt
        '
        Me.name_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.name_txt.Location = New System.Drawing.Point(170, 65)
        Me.name_txt.Name = "name_txt"
        Me.name_txt.Size = New System.Drawing.Size(155, 26)
        Me.name_txt.TabIndex = 26
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.MistyRose
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(79, 114)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(92, 20)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Last name :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.MistyRose
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(79, 68)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 20)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Name :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.MistyRose
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(79, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 20)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "ID  :"
        '
        'Status_custTableAdapter
        '
        Me.Status_custTableAdapter.ClearBeforeFill = True
        '
        'Status_custTableAdapter1
        '
        Me.Status_custTableAdapter1.ClearBeforeFill = True
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'New_Client
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(451, 340)
        Me.Controls.Add(Me.Statuscombo)
        Me.Controls.Add(Me.Stat_lbl)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.cell_txt)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.ID_lbl)
        Me.Controls.Add(Me.lname_txt)
        Me.Controls.Add(Me.name_txt)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.Name = "New_Client"
        Me.Text = "New Client"
        CType(Me.StatuscustBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sports_clubDataSet10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StatuscustBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Sports_clubDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Statuscombo As System.Windows.Forms.ComboBox
    Friend WithEvents Stat_lbl As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents cell_txt As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ID_lbl As System.Windows.Forms.Label
    Friend WithEvents lname_txt As System.Windows.Forms.TextBox
    Friend WithEvents name_txt As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Sports_clubDataSet1 As Sports_Club.Sports_clubDataSet1
    Friend WithEvents StatuscustBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Status_custTableAdapter As Sports_Club.Sports_clubDataSet1TableAdapters.Status_custTableAdapter
    Friend WithEvents Sports_clubDataSet10 As Sports_Club.Sports_clubDataSet10
    Friend WithEvents StatuscustBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents Status_custTableAdapter1 As Sports_Club.Sports_clubDataSet10TableAdapters.Status_custTableAdapter
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
End Class
