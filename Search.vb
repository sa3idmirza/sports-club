﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class Search
    Public str As String


    Public fromlogin As Boolean = False
    Public fromsupplier As Boolean = False
    Public fromPaymentEmp As Boolean = False
    Public fromrescust As Boolean = False
    Public fromresact As Boolean = False
    Public fromsupppay As Boolean = False
    Public fromRec As Boolean = False
    Public fromsupList As Boolean = False
    Public fromSuplistItem As Boolean = False
    Public frominvcust As Boolean = False
    Public frominvItem As Boolean = False
    Public frominvRes As Boolean = False
    Public frominvrep As Boolean = False
    Public fromrecrep As Boolean = False
    Public fromrepsup As Boolean = False
    Dim strq As String

    

    Private Sub param_txt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles param_txt.TextChanged
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        ds.Clear()
        If (fromresact) Then
            strq = str + param_txt.Text + "'"
        ElseIf (frominvRes) Then
           

            strq = str + "'" + param_txt.Text + "' , '" + Date_pick.Text + "' ,'" + New_Invoice.Cust_ID_lbl.Text + "'"

            Else
                strq = str + param_txt.Text + "%'"
            End If
            da = New SqlDataAdapter(strq, conx)
            da.Fill(ds)
            t = ds.Tables(0)
            DataGridView1.DataSource = t

            conx.Close()
    End Sub
    
    

    Private Sub DataGridView1_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridView1.MouseDoubleClick
        Dim x As Integer
        Try
            x = DataGridView1.CurrentCell.RowIndex
            If (fromlogin) Then
                New_User.emp_txt.Text = DataGridView1.Item(0, x).Value.ToString


            ElseIf (fromsupplier) Then

                Suppliers_list.namesupp_txt.Text = DataGridView1.Item(0, x).Value.ToString


            ElseIf (fromPaymentEmp) Then

                Payment_Employee.name_txt.Text = DataGridView1.Item(1, x).Value.ToString + " " + DataGridView1.Item(2, x).Value.ToString
                Payment_Employee.Salary_txt.Text = DataGridView1.Item(3, x).Value.ToString
                Payment_Employee.ID_lbl.Text = DataGridView1.Item(0, x).Value.ToString

            ElseIf (fromrescust) Then

                New_Reservation.Cust_ID_lbl.Text = DataGridView1.Item(0, x).Value.ToString
                Dim conx As SqlConnection
                conx = conSQL()
                conx.Open()
                Dim sqlcommand As New SqlCommand("select Cust_name from Customer_member where Cust_ID = '" + New_Reservation.Cust_ID_lbl.Text + "'", conx)
                sqlcommand.CommandType = CommandType.Text
                New_Reservation.Cust_name_txt.Text = sqlcommand.ExecuteScalar.ToString()
                conx.Close()

            ElseIf (fromresact) Then

                New_Reservation.Act_txt.Text = DataGridView1.Item(0, x).Value.ToString

            ElseIf (fromsupppay) Then

                Payment_Voucher.ID_lbl.Text = DataGridView1.Item(0, x).Value.ToString
                Dim conx As SqlConnection
                conx = conSQL()
                conx.Open()
                Dim sqlcommand As New SqlCommand("select Supplier_name from Suppliers where Supplier_ID = '" + Payment_Voucher.ID_lbl.Text + "'", conx)
                sqlcommand.CommandType = CommandType.Text
                Payment_Voucher.name_txt.Text = sqlcommand.ExecuteScalar.ToString()
                conx.Close()

            ElseIf (fromRec) Then

                Receipt.Cust_ID_lbl.Text = DataGridView1.Item(0, x).Value.ToString
                Dim conx As SqlConnection
                conx = conSQL()
                conx.Open()
                Dim sqlcommand As New SqlCommand("select Cust_name from Customer_member where Cust_ID = '" + Receipt.Cust_ID_lbl.Text + "'", conx)
                sqlcommand.CommandType = CommandType.Text
                Receipt.Cust_name_txt.Text = sqlcommand.ExecuteScalar.ToString()

            ElseIf (fromsupList) Then

                New_supplier_list.ID_lbl.Text = DataGridView1.Item(0, x).Value.ToString
                Dim conx As SqlConnection
                conx = conSQL()
                conx.Open()
                Dim sqlcommand As New SqlCommand("select Supplier_name from Suppliers where Supplier_ID = '" + New_supplier_list.ID_lbl.Text + "'", conx)
                sqlcommand.CommandType = CommandType.Text
                New_supplier_list.name_txt.Text = sqlcommand.ExecuteScalar.ToString()

                Dim sqlcommand1 As New SqlCommand("select Supplier_add from Suppliers where Supplier_ID = '" + New_supplier_list.ID_lbl.Text + "'", conx)
                sqlcommand1.CommandType = CommandType.Text
                New_supplier_list.address_txt.Text = sqlcommand1.ExecuteScalar.ToString()
                conx.Close()

            ElseIf (fromSuplistItem) Then

                Dim conx As SqlConnection
                conx = conSQL()
                conx.Open()
                New_supplier_list.Item_txt.Text = DataGridView1.Item(1, x).Value.ToString
                New_supplier_list.ID = Integer.Parse(DataGridView1.Item(0, x).Value.ToString)
                Dim sqlcommand As New SqlCommand("select Item_qtty from Items where Item_ID = '" + New_supplier_list.ID.ToString + "'", conx)
                sqlcommand.CommandType = CommandType.Text
                New_supplier_list.max_lbl.Text = sqlcommand.ExecuteScalar.ToString()

                Dim sqlcommand1 As New SqlCommand("select Item_cost from Items where Item_ID = '" + New_supplier_list.ID.ToString + "'", conx)
                sqlcommand1.CommandType = CommandType.Text
                New_supplier_list.cost_txt.Text = sqlcommand1.ExecuteScalar.ToString()

            ElseIf (frominvcust) Then

                New_Invoice.Cust_ID_lbl.Text = DataGridView1.Item(0, x).Value.ToString
                Dim conx As SqlConnection
                conx = conSQL()
                conx.Open()
                Dim sqlcommand As New SqlCommand("select Cust_name from Customer_member where Cust_ID = '" + New_Invoice.Cust_ID_lbl.Text + "'", conx)
                sqlcommand.CommandType = CommandType.Text
                New_Invoice.Cust_name_txt.Text = sqlcommand.ExecuteScalar.ToString()
                conx.Close()

            ElseIf (frominvItem) Then

                Dim conx As SqlConnection
                conx = conSQL()
                conx.Open()
                New_Invoice.Item_txt.Text = DataGridView1.Item(1, x).Value.ToString
                New_Invoice.ID = Integer.Parse(DataGridView1.Item(0, x).Value.ToString)
                Dim sqlcommand As New SqlCommand("select Item_qtty from Items where Item_ID = '" + New_Invoice.ID.ToString + "'", conx)
                sqlcommand.CommandType = CommandType.Text
                New_Invoice.max_lbl.Text = sqlcommand.ExecuteScalar.ToString()

                Dim sqlcommand1 As New SqlCommand("select Item_price from Items where Item_ID = '" + New_Invoice.ID.ToString + "'", conx)
                sqlcommand1.CommandType = CommandType.Text
                New_Invoice.cost_txt.Text = sqlcommand1.ExecuteScalar.ToString()

            ElseIf (frominvRes) Then

                Dim conx As SqlConnection
                conx = conSQL()
                conx.Open()
                New_Invoice.Item_txt.Text = DataGridView1.Item(4, x).Value.ToString
                New_Invoice.ID = Integer.Parse(DataGridView1.Item(0, x).Value.ToString)

                New_Invoice.max_lbl.Text = 1
                Dim sqlcommand As SqlCommand = New SqlCommand("getrentdetcost", conx)
                sqlcommand.CommandType = CommandType.StoredProcedure
                sqlcommand.Parameters.AddWithValue("@Res_ID", New_Invoice.ID)

                Dim sqlcommand1 As SqlCommand = New SqlCommand("getactcost", conx)
                sqlcommand1.CommandType = CommandType.StoredProcedure
                sqlcommand1.Parameters.AddWithValue("@Res_ID", New_Invoice.ID)

                Dim act As Integer
                Dim it As Integer
                If (sqlcommand.ExecuteScalar.ToString <> "") Then
                    act = sqlcommand.ExecuteScalar
                Else
                    act = 0
                End If
                If (sqlcommand1.ExecuteScalar.ToString <> "") Then
                    it = sqlcommand1.ExecuteScalar
                Else
                    it = 0
                End If
                New_Invoice.cost_txt.Text = act + it
                conx.Close()
            ElseIf (frominvrep) Then
                Invoice_Report.Cust_ID_lbl.Text = DataGridView1.Item(0, x).Value.ToString
                Dim conx As SqlConnection
                conx = conSQL()
                conx.Open()
                Dim sqlcommand As New SqlCommand("select Cust_name from Customer_member where Cust_ID = '" + Invoice_Report.Cust_ID_lbl.Text + "'", conx)
                sqlcommand.CommandType = CommandType.Text
                Invoice_Report.Cust_name_txt.Text = sqlcommand.ExecuteScalar.ToString()
                conx.Close()
            ElseIf (fromrecrep) Then
                Receipt_Report.Cust_ID_lbl.Text = DataGridView1.Item(0, x).Value.ToString
                Dim conx As SqlConnection
                conx = conSQL()
                conx.Open()
                Dim sqlcommand As New SqlCommand("select Cust_name from Customer_member where Cust_ID = '" + Receipt_Report.Cust_ID_lbl.Text + "'", conx)
                sqlcommand.CommandType = CommandType.Text
                Receipt_Report.Cust_name_txt.Text = sqlcommand.ExecuteScalar.ToString()
                conx.Close()
            ElseIf (fromrepsup) Then
                Suppliers_Report.Cust_ID_lbl.Text = DataGridView1.Item(0, x).Value.ToString
                Dim conx As SqlConnection
                conx = conSQL()
                conx.Open()
                Dim sqlcommand As New SqlCommand("select Supplier_name from Suppliers where Supplier_ID = '" + Suppliers_Report.Cust_ID_lbl.Text + "'", conx)
                sqlcommand.CommandType = CommandType.Text
                Suppliers_Report.Cust_name_txt.Text = sqlcommand.ExecuteScalar.ToString()
                conx.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Me.Close()
    End Sub

    

    Private Sub CheckBox1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If (CheckBox1.Checked = True) Then
             param_txt.Enabled = False
            Date_pick.Enabled = False
            Dim da As New SqlDataAdapter
            Dim ds As New DataSet
            Dim t As DataTable
            Dim conx As SqlConnection
            conx = conSQL()
            conx.Open()
            ds.Clear()
            strq = str + "'ALL' , '1/1/1970' ,'" + New_Invoice.Cust_ID_lbl.Text + "'"
            da = New SqlDataAdapter(strq, conx)
            da.Fill(ds)
            t = ds.Tables(0)
            DataGridView1.DataSource = t

            conx.Close()
        Else
            Dim da As New SqlDataAdapter
            Dim ds As New DataSet
            Dim t As DataTable
            Dim conx As SqlConnection
            conx = conSQL()
            conx.Open()
            ds.Clear()
            strq = str + "'" + param_txt.Text + "','" + Date_pick.Text + "','" + New_Invoice.Cust_ID_lbl.Text + "'"
            da = New SqlDataAdapter(strq, conx)
            da.Fill(ds)
            t = ds.Tables(0)
            DataGridView1.DataSource = t
            param_txt.Enabled = True
            Date_pick.Enabled = True
            conx.Close()

        End If
    End Sub

    
    Private Sub newcust_but_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles newcust_but.Click
        New_Client.MdiParent = Main
        New_Client.Show()
    End Sub

    Private Sub Search_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class