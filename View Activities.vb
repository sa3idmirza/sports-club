﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class View_Activities

    Private Sub View_Activities_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Sports_clubDataSet2.Activities' table. You can move, or remove it, as needed.
        Me.ActivitiesTableAdapter.Fill(Me.Sports_clubDataSet2.Activities)
        Dim conx As SqlConnection
        conx = conSQL()

        conx.Open()
        Try
            Dim command As SqlCommand = New SqlCommand("Select MAX(Act_ID) + 1 from Activities", conx)
            command.CommandType = CommandType.Text
            ID_lbl.Text = command.ExecuteScalar.ToString
        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
            If (Main.adminis = False) Then
                Button2.Enabled = False
            Else
                Button2.Enabled = True
            End If
        End Try
    End Sub


    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim conx As SqlConnection
        conx = conSQL()

        conx.Open()
        Try
            Dim command As SqlCommand = New SqlCommand("InsertIntoAct", conx)
            command.CommandType = CommandType.StoredProcedure

            command.Parameters.AddWithValue("@name", name_txt.Text)
            command.Parameters.AddWithValue("@price", price_txt.Text)
            command.ExecuteNonQuery()
            Me.View_Activities_Load(sender, e)
            name_txt.Clear()
            price_txt.Clear()
            name_txt.Select()
        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try
    End Sub


    Private Sub DataGridView1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridView1.MouseUp
        Dim x As Integer
        Try
            x = DataGridView1.CurrentCell.RowIndex
            ID_lbl.Text = x.ToString
            fillfield(x)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub fillfield(ByVal i As Integer)
        Try
            Dim r As DataRow
            Dim da As New SqlDataAdapter
            Dim ds As New DataSet
            Dim t As DataTable
            Dim conx As SqlConnection
            conx = conSQL()
            conx.Open()
            Dim strquery As String
            strquery = "select * from Activities "
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Items")
            t = ds.Tables(0)

            r = t.Rows(i)
            ID_lbl.Text = r.Item("Act_ID")
            name_txt.Text = r.Item("Act_desc")
            price_txt.Text = r.Item("Act_price")


            conx.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub Label6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label6.Click

    End Sub

    Private Sub Label4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label4.Click

    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub

    Private Sub Label2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label2.Click

    End Sub

    Private Sub ID_lbl_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ID_lbl.Click

    End Sub

    Private Sub price_txt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles price_txt.TextChanged

    End Sub

    Private Sub name_txt_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles name_txt.TextChanged

    End Sub
End Class