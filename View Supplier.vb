﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class View_Supplier

    Private Sub View_Supplier_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Sports_clubDataSet14.Suppliers' table. You can move, or remove it, as needed.
        Me.SuppliersTableAdapter.Fill(Me.Sports_clubDataSet14.Suppliers)


        Dim str As String
        str = "select Supplier_ID As 'ID' , Supplier_name as 'Name' , Supplier_cell as 'Cell Phone'  , Supplier_add as 'Address' from Suppliers "
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim sqlcommand As SqlCommand = New SqlCommand("select max(supplier_ID)+1 from suppliers", conx)
        sqlcommand.CommandType = CommandType.Text
        ID_lbl.Text = sqlcommand.ExecuteScalar.ToString

        da = New SqlDataAdapter(str, conx)
        da.Fill(ds)
        t = ds.Tables(0)
        DataGridView2.DataSource = t

        conx.Close()
    End Sub

   


    
    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles param_txt.TextChanged
        Dim str As String
        str = "select Supplier_ID As 'ID' , Supplier_name as 'Name' , Supplier_cell as 'Cell Phone'  , Supplier_add as 'Address' from Suppliers where Supplier_name like '%"
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()

        str += param_txt.Text + "%'"
        da = New SqlDataAdapter(str, conx)
        da.Fill(ds)
        t = ds.Tables(0)
        DataGridView2.DataSource = t

        conx.Close()
    End Sub

    Public Sub fillfield(ByVal i As Integer)
        Try
            Dim r As DataRow
            Dim da As New SqlDataAdapter
            Dim ds As New DataSet
            Dim t As DataTable
            Dim conx As SqlConnection
            conx = conSQL()
            conx.Open()
            Dim strquery As String
            strquery = "select * from Suppliers "
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds)
            t = ds.Tables(0)

            r = t.Rows(i)
            ID_lbl.Text = r.Item("Supplier_ID")
            name_txt.Text = r.Item("Supplier_name")
            cell_txt.Text = r.Item("Supplier_cell")
            address_txt.Text = r.Item("Supplier_add")


            conx.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub DataGridView2_MouseDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridView2.MouseDoubleClick
        Dim x As Integer
        x = DataGridView2.CurrentCell.RowIndex
        Dim r As DataRow
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim t As DataTable
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()
        Dim strquery As String
        strquery = "select * from Suppliers "
        da = New SqlDataAdapter(strquery, conx)
        da.Fill(ds)
        t = ds.Tables(0)
        r = t.Rows(x)
        Suppliers_list.MdiParent = Main
        Suppliers_list.Show()
        Suppliers_list.namesupp_txt.Text = r.Item("Supplier_ID")
        Suppliers_list.BringToFront()

    End Sub





    Private Sub DataGridView2_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridView2.MouseUp
        Dim x As Integer
        Try
            x = DataGridView2.CurrentCell.RowIndex
            ID_lbl.Text = x.ToString
            fillfield(x)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim conx As SqlConnection

        conx = conSQL()

        conx.Open()

        Dim command As SqlCommand = New SqlCommand("UpdateSupplier", conx)
        command.CommandType = CommandType.StoredProcedure

        command.Parameters.AddWithValue("@id", ID_lbl.Text)
        command.Parameters.AddWithValue("@name", name_txt.Text)
        command.Parameters.AddWithValue("@add", address_txt.Text)
        command.Parameters.AddWithValue("@cell", cell_txt.Text)
        command.ExecuteNonQuery()
        Me.View_Supplier_Load(sender, e)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim conx As SqlConnection

        conx = conSQL()

        conx.Open()
        Dim cmd As SqlCommand = New SqlCommand("select MAX (Supplier_ID) + 1 from Suppliers", conx)
        cmd.CommandType = CommandType.Text
        ID_lbl.Text = cmd.ExecuteScalar.ToString

        Button3.Visible = True


    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim conx As SqlConnection

        conx = conSQL()

        conx.Open()

        Dim command As SqlCommand = New SqlCommand("InsertIntoSupplier", conx)
        command.CommandType = CommandType.StoredProcedure

        name_txt.Clear()
        cell_txt.Clear()
        address_txt.Clear()

        command.Parameters.AddWithValue("@name", name_txt.Text)
        command.Parameters.AddWithValue("@add", address_txt.Text)
        command.Parameters.AddWithValue("@cell", cell_txt.Text)
        command.ExecuteNonQuery()
        Me.View_Supplier_Load(sender, e)
        Button3.Visible = False
    End Sub

    Private Sub DataGridView2_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView2.CellContentClick

    End Sub
End Class