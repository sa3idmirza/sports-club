﻿Imports System.Data.SqlClient
Imports System.Globalization
Imports System.Resources
Imports System.Threading
Imports System.IO
Public Class Rental_Items

    Private Sub Rental_Items_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Sports_clubDataSet11.Rental_Item' table. You can move, or remove it, as needed.
        Me.Rental_ItemTableAdapter.Fill(Me.Sports_clubDataSet11.Rental_Item)
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        Dim ta As DataTable
        Dim conx As SqlConnection
        conx = conSQL()
        conx.Open()

        If (Main.adminis) Then
            Button2.Enabled = True
            Button1.Enabled = True
            Button3.Enabled = True
            Button4.Enabled = True
        Else
            Button2.Enabled = False
            Button1.Enabled = False
            Button3.Enabled = False
            Button4.Enabled = False

        End If
        Dim command As SqlCommand = New SqlCommand("Select MAX(Rent_It_ID) + 1 from Rental_Item", conx)
        command.CommandType = CommandType.Text
        ID_lbl.Text = command.ExecuteScalar.ToString
         
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim conx As SqlConnection
        conx = conSQL()

        conx.Open()
        Try
            Dim command As SqlCommand = New SqlCommand("InsertIntoRentItm", conx)
            command.CommandType = CommandType.StoredProcedure

            command.Parameters.AddWithValue("@name", name_txt.Text)
            command.Parameters.AddWithValue("@price", price_txt.Text)

            command.ExecuteNonQuery()
            Me.Rental_Items_Load(sender, e)
            name_txt.Clear()
            price_txt.Clear()
            name_txt.Select()
            price_txt.Clear()

        Catch ex As Exception
            MsgBox(ex.Message)
            Throw
        Finally
            conx.Close()
        End Try
    End Sub
    Private Sub DataGridView1_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles DataGridView1.MouseUp
        Dim x As Integer
        Try
            x = DataGridView1.CurrentCell.RowIndex
            ID_lbl.Text = x.ToString
            fillfield(x)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub fillfield(ByVal i As Integer)
        Try
            Dim r As DataRow
            Dim da As New SqlDataAdapter
            Dim ds As New DataSet
            Dim t As DataTable
            Dim conx As SqlConnection
            conx = conSQL()
            conx.Open()
            Dim strquery As String
            strquery = "select * from Rental_Item "
            da = New SqlDataAdapter(strquery, conx)
            da.Fill(ds, "Rental_Item")
            t = ds.Tables(0)

            r = t.Rows(i)
            ID_lbl.Text = r.Item("Rent_It_ID")
            name_txt.Text = r.Item("Rent_It_Desc")
            price_txt.Text = r.Item("Rent_Price")


            conx.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Rental_Items_Load(sender, e)
        name_txt.Clear()
        price_txt.Clear()
        name_txt.Select()
        price_txt.Clear()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        name_txt.Clear()
        price_txt.Clear()
        name_txt.Select()
        price_txt.Clear()
    End Sub

   
End Class