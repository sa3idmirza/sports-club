﻿ 
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data
Public Class Main
    Public adminis As Boolean
    Public user_ID As Integer

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim ctl As Control
        Dim ctlMDI As MdiClient
        For Each ctl In Me.Controls
            Try
                ctlMDI = CType(ctl, MdiClient)
                ctlMDI.BackColor = Me.BackColor
            Catch exc As InvalidCastException
            End Try
        Next


        Dim Loginform As New Login
        Loginform.MdiParent = Me
        Loginform.Show()


    End Sub

    Private Sub LoginMenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoginMenu.Click
        Dim window As New Login
        window.MdiParent = Me
        window.Show()
    End Sub

    Private Sub ExitMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitMenuItem.Click
        End
    End Sub

    Private Sub NewClient_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewClient.Click

        New_Client.MdiParent = Me
        New_Client.Show()
    End Sub

    Private Sub ViewClient_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewClient.Click
        View_Client.MdiParent = Me
        View_Client.Show()
    End Sub

    Private Sub NewReservation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewReservation.Click
        New_Reservation.MdiParent = Me
        New_Reservation.Show()
    End Sub

    Private Sub ViewReservation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewReservation.Click

        Reservation_Archive.MdiParent = Me
        Reservation_Archive.Show()
    End Sub


    Private Sub ViewActivities_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewActivities.Click
        View_Activities.MdiParent = Me
        View_Activities.Show()
    End Sub


    Private Sub ViewItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewItems.Click
        Items.MdiParent = Me
        Items.Show()
    End Sub

    Private Sub StatusEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatusEmployee.Click
        Status_emp.MdiParent = Me
        Status_emp.Show()

    End Sub


    Private Sub CustStat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CustStat.Click
        Status_Cust.MdiParent = Me
        Status_Cust.Show()
    End Sub

    Private Sub EmployeeView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EmployeeView.Click
        View_Employee.MdiParent = Me
        View_Employee.Show()
    End Sub

    Private Sub ViewRentItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewRentItems.Click
        Rental_Items.MdiParent = Me
        Rental_Items.Show()
    End Sub

    Private Sub NewUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewUser.Click
        New_User.MdiParent = Me
        New_User.Show()

    End Sub

    Private Sub NewEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewEmployee.Click
        New_Employee.MdiParent = Me
        New_Employee.Show()
    End Sub

    Private Sub ViewSupplier_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewSupplier.Click
        View_Supplier.MdiParent = Me
        View_Supplier.Show()
    End Sub

    Private Sub ViewSuplliesList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewSuplliesList.Click
        Suppliers_list.MdiParent = Me
        Suppliers_list.Show()
    End Sub

    Private Sub NewPayment_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles NewPayment.Click
        Payment_Employee.MdiParent = Me
        Payment_Employee.Show()

    End Sub

    Private Sub paymentvouchermenu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles paymentvouchermenu.Click
        Payment_Voucher.MdiParent = Me
        Payment_Voucher.Show()
    End Sub

    Private Sub newReceipt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles newReceipt.Click
        Receipt.MdiParent = Me
        Receipt.Show()
    End Sub

    Private Sub BalanceReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BalanceReport.Click
        Balance.MdiParent = Me
        Balance.Show()
    End Sub

    Private Sub NewSupplierListToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Newsupplist.Click
        New_supplier_list.MdiParent = Me
        New_supplier_list.Show()
    End Sub

    Private Sub newSales_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles newSales.Click
        New_Invoice.MdiParent = Me
        New_Invoice.Show()
    End Sub

    Private Sub InvReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InvReport.Click
        Invoice_Report.MdiParent = Me
        Invoice_Report.Show()
         
    End Sub

    Private Sub RecReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RecReport.Click
        Receipt_Report.MdiParent = Me
        Receipt_Report.Show()
    End Sub

    Private Sub SuppReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SuppReport.Click
        Suppliers_Report.MdiParent = Me
        Suppliers_Report.Show()
    End Sub

    Private Sub PayrollReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PayrollReport.Click
        Report.MdiParent = Me
        Report.Show()
        Dim Invoicerepdoc As New ReportDocument
        Invoicerepdoc.Load("C:\Users\mahiro\Documents\Visual Studio 2010\Projects\Sports Club\Sports Club\Payroll_report.rpt")
        Report.CrystalReportViewer1.ReportSource = Invoicerepdoc

    End Sub
End Class