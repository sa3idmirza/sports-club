﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.Accounting = New System.Windows.Forms.ToolStripMenuItem()
        Me.newSales = New System.Windows.Forms.ToolStripMenuItem()
        Me.newReceipt = New System.Windows.Forms.ToolStripMenuItem()
        Me.BalanceReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReviewAndReprintToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InvReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.RecReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.SuppReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.PayrollReport = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmployeeItemMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewEmployee = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewUser = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewPayment = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusEmployee = New System.Windows.Forms.ToolStripMenuItem()
        Me.EmployeeView = New System.Windows.Forms.ToolStripMenuItem()
        Me.MmmToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewItems = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewRentItems = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewSupplier = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewSuplliesList = New System.Windows.Forms.ToolStripMenuItem()
        Me.paymentvouchermenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.Newsupplist = New System.Windows.Forms.ToolStripMenuItem()
        Me.MmToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewClient = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewReservation = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewReservation = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewClient = New System.Windows.Forms.ToolStripMenuItem()
        Me.ViewActivities = New System.Windows.Forms.ToolStripMenuItem()
        Me.CustStat = New System.Windows.Forms.ToolStripMenuItem()
        Me.LoginMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.Control
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Accounting, Me.MmmToolStripMenuItem, Me.MmToolStripMenuItem1, Me.LoginMenu, Me.ExitMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(846, 24)
        Me.MenuStrip1.TabIndex = 4
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'Accounting
        '
        Me.Accounting.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.newSales, Me.newReceipt, Me.BalanceReport, Me.ReviewAndReprintToolStripMenuItem, Me.EmployeeItemMenu})
        Me.Accounting.Name = "Accounting"
        Me.Accounting.Size = New System.Drawing.Size(81, 20)
        Me.Accounting.Text = "Accounting"
        '
        'newSales
        '
        Me.newSales.Name = "newSales"
        Me.newSales.Size = New System.Drawing.Size(183, 22)
        Me.newSales.Text = "New Sales Invoice"
        '
        'newReceipt
        '
        Me.newReceipt.Name = "newReceipt"
        Me.newReceipt.Size = New System.Drawing.Size(183, 22)
        Me.newReceipt.Text = "New Receipt"
        '
        'BalanceReport
        '
        Me.BalanceReport.Name = "BalanceReport"
        Me.BalanceReport.Size = New System.Drawing.Size(183, 22)
        Me.BalanceReport.Text = "Balance View/Report"
        '
        'ReviewAndReprintToolStripMenuItem
        '
        Me.ReviewAndReprintToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InvReport, Me.RecReport, Me.SuppReport, Me.PayrollReport})
        Me.ReviewAndReprintToolStripMenuItem.Name = "ReviewAndReprintToolStripMenuItem"
        Me.ReviewAndReprintToolStripMenuItem.Size = New System.Drawing.Size(183, 22)
        Me.ReviewAndReprintToolStripMenuItem.Text = "Review And Print"
        '
        'InvReport
        '
        Me.InvReport.Name = "InvReport"
        Me.InvReport.Size = New System.Drawing.Size(155, 22)
        Me.InvReport.Text = "Invoice Report"
        '
        'RecReport
        '
        Me.RecReport.Name = "RecReport"
        Me.RecReport.Size = New System.Drawing.Size(155, 22)
        Me.RecReport.Text = "Receipt Report"
        '
        'SuppReport
        '
        Me.SuppReport.Name = "SuppReport"
        Me.SuppReport.Size = New System.Drawing.Size(155, 22)
        Me.SuppReport.Text = "Supplier Report"
        '
        'PayrollReport
        '
        Me.PayrollReport.Name = "PayrollReport"
        Me.PayrollReport.Size = New System.Drawing.Size(155, 22)
        Me.PayrollReport.Text = "Payroll Report"
        '
        'EmployeeItemMenu
        '
        Me.EmployeeItemMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewEmployee, Me.NewUser, Me.NewPayment, Me.StatusEmployee, Me.EmployeeView})
        Me.EmployeeItemMenu.Name = "EmployeeItemMenu"
        Me.EmployeeItemMenu.Size = New System.Drawing.Size(183, 22)
        Me.EmployeeItemMenu.Text = "Employee"
        '
        'NewEmployee
        '
        Me.NewEmployee.Name = "NewEmployee"
        Me.NewEmployee.Size = New System.Drawing.Size(161, 22)
        Me.NewEmployee.Text = "New Employee"
        '
        'NewUser
        '
        Me.NewUser.Name = "NewUser"
        Me.NewUser.Size = New System.Drawing.Size(161, 22)
        Me.NewUser.Text = "New User"
        '
        'NewPayment
        '
        Me.NewPayment.Name = "NewPayment"
        Me.NewPayment.Size = New System.Drawing.Size(161, 22)
        Me.NewPayment.Text = "New Payment"
        '
        'StatusEmployee
        '
        Me.StatusEmployee.Name = "StatusEmployee"
        Me.StatusEmployee.Size = New System.Drawing.Size(161, 22)
        Me.StatusEmployee.Text = "Status Employee"
        '
        'EmployeeView
        '
        Me.EmployeeView.Name = "EmployeeView"
        Me.EmployeeView.Size = New System.Drawing.Size(161, 22)
        Me.EmployeeView.Text = "View Employee"
        '
        'MmmToolStripMenuItem
        '
        Me.MmmToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ViewItems, Me.ViewRentItems, Me.ViewSupplier, Me.ViewSuplliesList, Me.paymentvouchermenu, Me.Newsupplist})
        Me.MmmToolStripMenuItem.Name = "MmmToolStripMenuItem"
        Me.MmmToolStripMenuItem.Size = New System.Drawing.Size(48, 20)
        Me.MmmToolStripMenuItem.Text = "Stock"
        '
        'ViewItems
        '
        Me.ViewItems.Name = "ViewItems"
        Me.ViewItems.Size = New System.Drawing.Size(195, 22)
        Me.ViewItems.Text = "Items "
        '
        'ViewRentItems
        '
        Me.ViewRentItems.Name = "ViewRentItems"
        Me.ViewRentItems.Size = New System.Drawing.Size(195, 22)
        Me.ViewRentItems.Text = "Rental Items"
        '
        'ViewSupplier
        '
        Me.ViewSupplier.Name = "ViewSupplier"
        Me.ViewSupplier.Size = New System.Drawing.Size(195, 22)
        Me.ViewSupplier.Text = "View Suppliers"
        '
        'ViewSuplliesList
        '
        Me.ViewSuplliesList.Name = "ViewSuplliesList"
        Me.ViewSuplliesList.Size = New System.Drawing.Size(195, 22)
        Me.ViewSuplliesList.Text = "Suppliers List"
        '
        'paymentvouchermenu
        '
        Me.paymentvouchermenu.Name = "paymentvouchermenu"
        Me.paymentvouchermenu.Size = New System.Drawing.Size(195, 22)
        Me.paymentvouchermenu.Text = "New Payment Voucher"
        '
        'Newsupplist
        '
        Me.Newsupplist.Name = "Newsupplist"
        Me.Newsupplist.Size = New System.Drawing.Size(195, 22)
        Me.Newsupplist.Text = "New Supplier List"
        '
        'MmToolStripMenuItem1
        '
        Me.MmToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewClient, Me.NewReservation, Me.ViewReservation, Me.ViewClient, Me.ViewActivities, Me.CustStat})
        Me.MmToolStripMenuItem1.Name = "MmToolStripMenuItem1"
        Me.MmToolStripMenuItem1.Size = New System.Drawing.Size(126, 20)
        Me.MmToolStripMenuItem1.Text = "Reservations/Clients"
        '
        'NewClient
        '
        Me.NewClient.Name = "NewClient"
        Me.NewClient.Size = New System.Drawing.Size(178, 22)
        Me.NewClient.Text = "New Client"
        '
        'NewReservation
        '
        Me.NewReservation.Name = "NewReservation"
        Me.NewReservation.Size = New System.Drawing.Size(178, 22)
        Me.NewReservation.Text = "New Reservation"
        '
        'ViewReservation
        '
        Me.ViewReservation.Name = "ViewReservation"
        Me.ViewReservation.Size = New System.Drawing.Size(178, 22)
        Me.ViewReservation.Text = "Reservation Archive"
        '
        'ViewClient
        '
        Me.ViewClient.Name = "ViewClient"
        Me.ViewClient.Size = New System.Drawing.Size(178, 22)
        Me.ViewClient.Text = "View Client "
        '
        'ViewActivities
        '
        Me.ViewActivities.Name = "ViewActivities"
        Me.ViewActivities.Size = New System.Drawing.Size(178, 22)
        Me.ViewActivities.Text = "Activities "
        '
        'CustStat
        '
        Me.CustStat.Name = "CustStat"
        Me.CustStat.Size = New System.Drawing.Size(178, 22)
        Me.CustStat.Text = "Status Client"
        '
        'LoginMenu
        '
        Me.LoginMenu.Name = "LoginMenu"
        Me.LoginMenu.Size = New System.Drawing.Size(49, 20)
        Me.LoginMenu.Text = "Login"
        '
        'ExitMenuItem
        '
        Me.ExitMenuItem.Name = "ExitMenuItem"
        Me.ExitMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.ExitMenuItem.Text = "Exit"
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkBlue
        Me.ClientSize = New System.Drawing.Size(846, 381)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.IsMdiContainer = True
        Me.Name = "Main"
        Me.Text = "The Club"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents Accounting As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents newSales As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents newReceipt As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BalanceReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReviewAndReprintToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InvReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RecReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SuppReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PayrollReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmployeeItemMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewEmployee As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewUser As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewPayment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusEmployee As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EmployeeView As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MmmToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewItems As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewRentItems As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewSupplier As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewSuplliesList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MmToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewClient As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewReservation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewReservation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewClient As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LoginMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ViewActivities As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents paymentvouchermenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CustStat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Newsupplist As System.Windows.Forms.ToolStripMenuItem
End Class
